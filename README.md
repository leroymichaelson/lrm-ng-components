# LRM   Angular Components Workspace

There are two projects in this workspace.  The Angular Components Library 
and the Tutorial web application that shows how to use the library.

## Angular Components Library

To build the library, run `npm publish:all`.  

## Tutorial

To run the Tutorial application, run `ng install` followed by `ng serve' and visit
http://localhost:2400. 

The app will automatically reload if you change any of the source files.

