constructor(private popoverService: PopoverService) { }

show(template: TemplateRef<any>, element: HTMLElement) {
  this.popoverService.open({
    origin: element,
    content: template
  });
}
