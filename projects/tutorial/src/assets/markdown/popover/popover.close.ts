private popoverRef: PopoverRef<any>;

show(element: HTMLElement, template: TemplateRef<any>) {
  this.popoverRef = this.popoverService.open({
    origin: element,
    content: template,
  });
}

close() {
  if ( this.popoverRef ) {
    this.popoverRef.close();
  }
}
