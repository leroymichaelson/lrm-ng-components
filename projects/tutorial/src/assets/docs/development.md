This page is for developers modifying the component library and/or the component tutorial web site.
      
There are instructions here for doing one of the two things:

- Add to or modify a LRM Component in the library
- Add to or modify the Tutorial site with additional usage instructions.

## Updating Tutorial Site

Updating the tutorial site (this site) can be done freely and frequently.

### In Your Private Branch

- `npm run precheck`
- Commit code
- Merge code to main branch

### In The Main Branch

- `npm run precheck` - This helps make sure no files have been missed
- Commit using a standard descriptive commit message describing the change.  

Note that this does **not** change the published component package in the npm registry.


----------------------------------
## Publishing New Component Library Version

Publishing a new version of the component library should be **done with great care**.

### In Your Private Branch

- `npm run precheck`
- Commit code
- Merge code to main branch

### In The Main Branch

- `npm run precheck` - This helps make sure no files have been missed
- Depending on the versioning priority, do one of the following.
  - `npm run publish:patch`
  - `npm run publish:minor` 
  - `npm run publish:major` - Extremely Rare!
- The previous command will increment the version number, build the npm package
  and publish it to our npm server.  You will be able to see the version number
  displayed somewhere in the feedback in your command window.
- Commit using a message of "Version x.y.z - <message>"  
   
## Versioning

Versioning is done by the principles of [Semantic Versioning](https://docs.npmjs.com/about-semantic-versioning).

The version numbers are in the format **X.Y.Z**

**X** is incremented for changes that break backwards compatibility.  This will rarely be used.  (Y and Z are reset to 0)

**Y** is incremented for typical new features. (Z is set to 0)

**Z** is incremented for bug fixes.

## Development Process

### Responsibilities When Making Changes

- Document the component/feature on the appropriate page(s), adding pages where necessary

- Update the Release Notes http://localhost:4200/releasenotes

- Update any Known Issues http://localhost:4200/issues

- Update the index as necessary http://localhost:4200/home

- Update/add the parameters table for the component(s) (Found in parameters.service.ts)

- Make sure the commit messages starts with "Version x.y.z - description"

### Typical Sequence for Updates

- Make the changes/additions

- Have the commit ready

- Run "npm run publish:patch" (or minor) (publishes to npm)

  - Note: If you are only making changes to the tutorial site and not to the library, please DO NOT publish.

- Verify that my commit message matches the actually published version number

- Verify that my release notes version matches the actually published version number

- Commit

- Wait for the tutorial build is finished

- Verify that the tutorial web site updated properly
