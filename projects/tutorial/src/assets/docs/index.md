## Index

The components available in the LRM Component Library are:

- Actions
  - [Actions](actions) - A library of common actions
  - [Action Container](actioncontainer) - A container for managing actions
  
- Inputs
  - [lrm-input-text](inputs#text) - A standard text input
  - [lrm-input-textarea](inputs#textarea) - A standard text area
  - [lrm-input-boolean](inputs#boolean) - A checkbox for boolean values
  - [lrm-input-password](inputs#password) - A textbox for password input
  - [lrm-input-email](inputs#email) - An email input
  - [lrm-input-date](inputs#date) - A popup date input
  - [lrm-input-phone](inputs#phone) - A phone number input
  - [lrm-input-ssn](inputs#ssn) - A social security number input
  - [lrm-input-time](inputs#time) - A time text input
  - [lrm-input-datetime](inputs#datetime) -  A date input and text input combined

- Selections
  - [lrm-radioset](selections#radioset) - Choosing one from a few
  - [lrm-checkboxset](selections#checkboxset) - Choosing many from a few
  - [lrm-dropdown-single](selections#dropdown-single) - Choosing one from many
  - [lrm-dropdown-multi](selections#dropdown-multi) - Choosing many from many

- Layout
  - [lrm-page](page)
  - [lrm-card](page)
  - [lrm-tabbed-card](page#tabbedcard)
  - [lrm-loading-container](loadingindicator) - High level loading indicator
  - [lrm-action-container](actioncontainer) - A container for managing actions

- Outputs
  - [lrm-output-phone](outputs#phone) - A phone number output formatter
  
- Others
  - [lrm-confirmation-dialog](confirmationdialog) - A modal asking the user a yes/no question
  - [lrm-dialog](dialog) - A modal to be used (rarely) when the `lrm-confirmation-dialog` will not work

Additional components are in development and are outlined 
in the [Roadmap](roadmap)

