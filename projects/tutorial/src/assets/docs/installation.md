## Using the LRM Component Library in an Application

## Configure Project to use NPM Local Server

In order to find the XYZ Components, the project must be configured to use the
local NPM server. This is accomplished by adding a file named `.npmrc` in the 
root directory of the project.  The contents will be as follows:

```shell script
  registry=url-of-registty
  always-auth=true
  email=internal@bi.com
  strict-ssl=false
  SASS_FORCE_BUILD=false
  package-lock=false
```

## Install Component Library

```shell script
npm install lrm-ng-components --save
```

In package.json, change the version to "latest", or whichever version is desired.

```json
"lrm-ng-components": "latest",
```
## install dependent libraries

```shell script
npm install angular2-text-mask --save
```

Make sure that after running the installation, the package.json lists the dependent libraries.

```shell script
npm install @angular/cdk --save
```


## app.module.ts

In order for PrimeNg animations to work, the `BrowserAnimationsModule`
must be included.

```typescript
@NgModule({
  declarations: [
  ],
  imports: [
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## angular.json

The following styles must be added to the application.

```json
{
  "styles": [
    "node_modules/lrm-ng-components/styles/styles.scss",
  ]
}
```
