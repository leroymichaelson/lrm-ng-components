## Overview

The LRM Component library provides a set of reusable components for building
Angular applications.

Some of the goals for this library are:

- Reduce the lines of application code
- Promote a consistent user interface
- Simplified programmer's interface

