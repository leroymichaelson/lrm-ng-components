import {Component, OnInit} from '@angular/core';

declare function require(moduleName: string): any;

const { version: appVersion } = require('../../../../package.json');

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public appVersion;

  constructor() {
    this.appVersion = appVersion;
  }

  ngOnInit() {
  }
}
