import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LRMComponentsModule } from '../../../components/src/lib/lrm-components.module';
import { SampleFormComponent } from './pages/sample-form/sample-form.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormSamplerComponent } from './pages/form-sampler/form-sampler.component';
import { RequiredIndicatorComponent } from './pages/required-indicator/required-indicator.component';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClientModule } from '@angular/common/http';
import { PageComponent } from './pages/layout/page/page.component';
import { Layout01Component } from './pages/layout/layout01/layout01.component';
import { Layout02Component } from './pages/layout/layout02/layout02.component';
import { ConfirmationDialogComponent } from './pages/confirmation-dialog/confirmation-dialog.component';
import { LoadingIndicatorComponent } from './pages/loading-indicator/loading-indicator.component';
import { InstallationComponent } from './pages/installation/installation.component';
import { RoadmapComponent } from './pages/roadmap/roadmap.component';
import { ReleaseNotesComponent } from './pages/release-notes/release-notes.component';
import { HomeComponent } from './pages/home/home.component';
import { IndexComponent } from './pages/index/index.component';
import { ActionsComponent } from './pages/actions/actions.component';
import { InputsComponent } from './pages/inputs/inputs.component';
import { SelectionsComponent } from './pages/selections/selections.component';
import { ParametersComponent } from './parameters/parameters.component';
import { DevelopmentComponent } from './pages/development/development.component';
import { IssuesComponent } from './pages/issues/issues.component';
import { ActionContainerComponent } from './pages/action-container/action-container.component';
import { DialogComponent } from './pages/dialog/dialog.component';
import { PageWithToolsComponent } from './pages/layout/page-with-tools/page-with-tools.component';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { StuffComponent } from './pages/popover/stuff/stuff.component';
import { PopoverComponent } from './pages/popover/popover.component';
import { OutputsComponent } from './pages/outputs/outputs.component';
import { TabbedCardComponent } from './pages/layout/tabbed-card/tabbed-card.component';

const pages = [
  ActionsComponent,
  FormSamplerComponent,
  HomeComponent,
  IndexComponent,
  InstallationComponent,
  Layout01Component,
  Layout02Component,
  LoadingIndicatorComponent,
  ConfirmationDialogComponent,
  PageComponent,
  RequiredIndicatorComponent,
  SampleFormComponent,
  ReleaseNotesComponent,
  RoadmapComponent,
  InputsComponent,
  SelectionsComponent,
  ParametersComponent,
  DevelopmentComponent,
  IssuesComponent,
  ActionContainerComponent,
  DialogComponent,
  PageWithToolsComponent,
  PopoverComponent,
  StuffComponent,
  OutputsComponent
];

@NgModule({
  declarations: [AppComponent, pages, TabbedCardComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LRMComponentsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MarkdownModule.forRoot(),
    MatFormFieldModule,
    MatInputModule,
    OverlayModule
  ],
  entryComponents: [StuffComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
