import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tut-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss']
})
export class ParametersComponent implements OnInit {

  @Input() name: string;
  @Input() parameters: IParameter[];

  constructor() { }

  ngOnInit() {
  }

}
