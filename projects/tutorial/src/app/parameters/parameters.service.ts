import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {

  common: IParameter[] = [
    {
      name: 'label',
      type: 'string',
      desc: 'Text describing the input'
    },
    {
      name: 'required',
      type: 'boolean',
      default: 'false',
    },
    {
      name: 'disabled',
      type: 'boolean',
      default: 'false',
    },
    {
      name: 'hint',
      type: 'string',
      desc: 'A suggestion to guide the user'
    }
  ];

  outputCommon: IParameter[] = [
    {
      name: 'value',
      type: 'string',
      desc: 'Value that needs to be formatted'
    }
  ];

  confirmationDialog: IParameter[] = [
    {
      name: 'visible',
      type: 'boolean',
      default: 'false',
      // tslint:disable-next-line:max-line-length
      desc: 'Set this to true to display the dialog. Since this is bound both ways, your variable will be set to false when a button is pressed and the dialog is closed.'
    },
    {
      name: 'header',
      type: 'string',
      default: '"Confirm"',
      desc: 'The top header of the dialog'
    },
    {
      name: 'positiveText',
      type: 'string',
      default: '"Yes"',
      desc: 'The text shown on the positive (and most typical) answer to the question'
    },
    {
      name: 'clickPositive',
      type: 'method',
      desc: 'Action to be taken when the positive answer is selected'
    },
    {
      name: 'showPositive',
      type: 'boolean',
      default: 'true',
      desc: 'If true, the positive button is shown'
    },
    {
      name: 'negativeText',
      type: 'string',
      default: '"No"',
      desc: 'The text shown on the negative (and least typical) answer to the question'
    },
    {
      name: 'clickNegative',
      type: 'method',
      desc: 'Action to be taken when the negative answer is selected'
    },
    {
      name: 'showNegative',
      type: 'boolean',
      default: 'true',
      desc: 'If true, the negative button is shown'
    },
    {
      name: 'severity',
      type: 'string',
      default: '"warning"',
      desc: 'Changes the severity of the question. The optional value is "error"'
    }
  ];

  dialog: IParameter[] = [
    {
      name: 'visible',
      type: 'boolean',
      default: 'false',
      // tslint:disable-next-line:max-line-length
      desc: 'Set this to true to display the dialog. Since this is bound both ways, your variable will be set to false when a button is pressed and the dialog is closed.'
    },
    {
      name: 'header',
      type: 'string',
      default: '"Confirm"',
      desc: 'The top header of the dialog'
    },
    {
      name: 'width',
      type: 'string',
      desc: 'width of the dialog'
    }
  ];

  popover: IParameter[] = [
    {
      name: 'origin',
      type: 'HTMLElement',
      desc: 'The DOM object to which the popoever will attach',
    },
    {
      name: 'content',
      type: 'TemplateRef',
      desc: 'The template to be displayed in the popover',
    },
    {
      name: 'width',
      type: 'string',
      desc: '',
    },
    {
      name: 'height',
      type: 'string',
      desc: '',
    },
  ];

  text: IParameter[];
  password: IParameter[];
  textarea: IParameter[];
  boolean: IParameter[];
  email: IParameter[];
  selections: IParameter[];
  dropdownSingle: IParameter[];
  dropdownMulti: IParameter[];
  radioset: IParameter[];
  checkboxset: IParameter[];
  date: IParameter[];
  time: IParameter[];
  dateTime: IParameter[];
  phone: IParameter[];
  outputPhone: IParameter[];
  ssn: IParameter[];
  selectionOption: IParameter[];

  constructor() {
    this.text = this.common.concat([
      {
        name: 'placeholder',
        type: 'string',
      },
      {
        name: 'minlength',
        type: 'number',
      },
      {
        name: 'maxlength',
        type: 'number',
      },
      {
        name: 'pattern',
        type: 'string',
        desc: 'A regular expression that the input value must match'
      },
      {
        name: 'patternError',
        type: 'string',
        desc: 'Message to be displayed when the pattern validator fails',
        default: '"Does not match pattern"',
      }
    ]);

    this.boolean = this.common.concat();
    this.password = this.text.concat();
    this.email = this.text.concat();
    this.selections = this.common.concat();

    this.dropdownSingle = this.selections.concat([
      {
        name: 'placeholder',
        type: 'string',
        desc: 'Value to be displayed if no input is provided'
      }
    ]);

    this.dropdownMulti = this.selections.concat();
    this.radioset = this.selections.concat();
    this.checkboxset = this.selections.concat();
    this.date = this.common.concat();

    this.phone = this.common.concat([
      {
        name: 'country',
        type: 'string',
        desc: 'A string value representing the ISO2 code of the default selected country the phone number should be formatted for',
        default: 'US'
      }
    ]);

    this.outputPhone = this.outputCommon.concat([
      {
        name: 'country',
        type: 'string',
        desc: 'A string value representing the ISO2 code of the default country the phone number should be formatted for, ' +
          'in case it can not be determined from the given value',
        default: 'US'
      }
    ]);

    this.ssn = this.common.concat();

    this.time = this.common.concat([
      {
        name: 'defaultTime',
        type: 'string',
        desc: 'A string value of format 99:99 to use as a default time',
        default: '00:00'
      }
    ]);

    this.textarea = this.common.concat([
      {
        name: 'maxlength',
        type: 'number',
        desc: 'The maximum characters allowed'
      },
      {
        name: 'placeholder',
        type: 'string',
        desc: 'The placeholder for the text area'
      },
      {
        name: 'rows',
        type: 'number',
        desc: 'The number of rows of the text area',
        default: '5'
      }
    ]);

    this.dateTime = [
      {
        name: 'dateLabel',
        type: 'string',
        desc: 'Text describing the date input'
      },
      {
        name: 'timeLabel',
        type: 'string',
        desc: 'Text describing the time input'
      },
      {
        name: 'required',
        type: 'boolean',
        default: 'false',
      },
      {
        name: 'disabled',
        type: 'boolean',
        default: 'false',
      },
      {
        name: 'dateHint',
        type: 'string',
        desc: 'A suggestion to guide the user with the date input'
      },
      {
        name: 'timeHint',
        type: 'string',
        desc: 'A suggestion to guide the user with the time input'
      },
      {
        name: 'defaultTime',
        type: 'string',
        desc: 'A string value of format 99:99 to use as a default time',
        default: '00:00'
      }
    ];

    this.selectionOption = ([
      {
        name: 'value',
        type: 'any (string|number|object)',
        desc: 'A value used to identify the option when it is different from the visible text',
        default: 'The transcluded text',
      }
    ]);
  }
}
