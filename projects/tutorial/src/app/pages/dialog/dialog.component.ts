import { Component, OnInit } from '@angular/core';
import {ParametersService} from '../../parameters/parameters.service';

@Component({
  selector: 'tut-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
//  providers: [MessageService]
})
export class DialogComponent {

  displayLrmDialog = false;

  constructor(
    public parameters: ParametersService,
//    private messageService: MessageService
  ) { }

  showLrmDialog() {
    this.displayLrmDialog = true;
  }

  onYes() {
    console.log( 'Yes' );
//    this.messageService.add({severity: 'success', summary: 'Yes', detail: 'Positive answer was chosen'});
    this.displayLrmDialog = false;
  }

  onNo() {
//    this.messageService.add({severity: 'error', summary: 'No', detail: 'Negative answer was chosen'});
    this.displayLrmDialog = false;
  }

  onMaybe() {
//    this.messageService.add({severity: 'info', summary: 'Hmmmm', detail: 'Let\'s do something else'});
    this.displayLrmDialog = false;
  }

}
