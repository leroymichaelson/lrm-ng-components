import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tut-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss']
})
export class LoadingIndicatorComponent implements OnInit {
  isLoading = false;
  hasError = false;

  constructor() { }

  ngOnInit() {
  }

}
