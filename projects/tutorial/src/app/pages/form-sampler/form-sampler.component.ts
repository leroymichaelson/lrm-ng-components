import { Component, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'tut-form-sampler',
  templateUrl: './form-sampler.component.html',
  styleUrls: ['./form-sampler.component.scss'],
})
export class FormSamplerComponent implements OnInit, AfterViewChecked {
  constructor(private changeDetector: ChangeDetectorRef) {}

  hint = '';

  form = new FormGroup({
    allDisabled: new FormControl(false),
    allRequired: new FormControl(false),

    username: new FormControl(''),
    password: new FormControl(''),
    sendWelcomeEmail: new FormControl(true),
    email: new FormControl('', null, this.validateDeactivation.bind(this)),
    alphaOnly: new FormControl(''),
    crust: new FormControl('', null, this.validateDeactivation.bind(this)),
    toppings: new FormControl('', null, this.validateDeactivation.bind(this)),
    sauce: new FormControl(''),
    drinks: new FormControl(''),
    deliveryDate: new FormControl(''),
    time: new FormControl(''),
    deliveryPhone: new FormControl(''),
    ssn: new FormControl(''),
  });

  allRequired: boolean;
  allDisabled: boolean;

  private async validateDeactivation(control: FormControl) {
    return new Promise((resolve) => {
      resolve(null);
    });
  }

  populate() {
    this.form.controls.username.setValue('FredSmith');
    this.form.controls.password.setValue('mybadpassword');
    this.form.controls.sendWelcomeEmail.setValue(true);
    this.form.controls.email.setValue('fsmith@abc.com');
    this.form.controls.alphaOnly.setValue('abcdefg');
    this.form.controls.crust.setValue('Thick');
    this.form.controls.toppings.setValue(['Sausage', 'Onions']);
    this.form.controls.sauce.setValue('Garlic');
    this.form.controls.drinks.setValue(['Pepsi', '4']);
    this.form.controls.deliveryDate.setValue(new Date());
    this.form.controls.deliveryPhone.setValue('3037891345');
    this.form.controls.ssn.setValue('333445555');
    this.form.controls.time.setValue(new Date());
  }

  clear() {
    this.form.controls.username.setValue('');
    this.form.controls.password.setValue('');
    this.form.controls.sendWelcomeEmail.setValue(true);
    this.form.controls.email.setValue('');
    this.form.controls.alphaOnly.setValue('');
    this.form.controls.crust.setValue('');
    this.form.controls.toppings.setValue([]);
    this.form.controls.sauce.setValue('');
    this.form.controls.drinks.setValue([]);
    this.form.controls.deliveryDate.setValue(null);
    this.form.controls.deliveryPhone.setValue('');
    this.form.controls.ssn.setValue('');
    this.form.controls.time.setValue(null);
  }

  ngOnInit() {
    this.form.valueChanges.subscribe((formValue) => {
      this.allRequired = formValue.allRequired;
      this.allDisabled = formValue.allDisabled;
    });
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  toggleDisabled(value) {
    Object.keys(this.form.controls).forEach((key) => {
      if (key !== 'allDisabled') {
        if (value) {
          this.form.controls[key].disable();
        } else {
          this.form.controls[key].enable();
        }
      }
    });
  }

  submit() {}

  showHints() {
    this.hint = 'This is what a hint looks like';
  }

  clearHints() {
    this.hint = '';
  }
}
