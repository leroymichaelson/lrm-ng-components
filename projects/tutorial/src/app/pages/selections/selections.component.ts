import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ParametersService} from '../../parameters/parameters.service';

interface ITopping {
  name: string;
  calories: number;
}

@Component({
  selector: 'tut-selections',
  templateUrl: './selections.component.html',
  styleUrls: ['./selections.component.scss']
})
export class SelectionsComponent implements OnInit {

  form = new FormGroup({
    crust: new FormControl(''),
    toppings: new FormControl(''),
    sauce: new FormControl(''),
    drinks: new FormControl(''),
    altToppings: new FormControl(''),
  });

  toppings: ITopping[] = [
    {
      name: 'Pepperoni',
      calories: 120
    },
    {
      name: 'Pineapple',
      calories: 175
    },
    {
      name: 'Garlic',
      calories: 23
    },
  ];

  constructor(public parameters: ParametersService) { }

  ngOnInit(): void {
  }

  populateToppings() {
    this.form.controls.altToppings.setValue( this.toppings[2]);
  }
}
