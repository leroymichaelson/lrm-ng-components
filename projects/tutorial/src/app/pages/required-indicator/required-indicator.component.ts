import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tut-required-indicator',
  templateUrl: './required-indicator.component.html',
  styleUrls: ['./required-indicator.component.scss']
})
export class RequiredIndicatorComponent implements OnInit {
  checked = false;

  constructor() { }

  ngOnInit() {
  }

}
