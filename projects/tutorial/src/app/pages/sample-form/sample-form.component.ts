import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'tut-sample-form',
  templateUrl: './sample-form.component.html',
  styleUrls: ['./sample-form.component.scss']
})
export class SampleFormComponent implements OnInit {

  form = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl(''),
    sendUserNameEmail: new FormControl(true),

    address1: new FormControl(''),
    address2: new FormControl(''),
    address3: new FormControl(''),
    city: new FormControl(''),
    county: new FormControl(''),
    state: new FormControl(''),
    postalCode: new FormControl(''),

    sendToEmail: new FormControl(true),
    sendToPager: new FormControl(false),
    sendToOtherEmail: new FormControl(false),
    otherEmail: new FormControl(''),
    shareWithOthers: new FormControl(false),

    favoriteColor: new FormControl('Green'),
    favoriteColors: new FormControl('Blue'),
    pizzaToppings: new FormControl(''),

  });

  constructor() { }

  ngOnInit() {
  }

}
