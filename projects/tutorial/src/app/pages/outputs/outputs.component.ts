import { Component } from '@angular/core';
import { ParametersService } from '../../parameters/parameters.service';

@Component({
  selector: 'tut-outputs',
  templateUrl: './outputs.component.html',
  styleUrls: ['./outputs.component.scss'],
})
export class OutputsComponent {
  constructor(public parameters: ParametersService) {}
}
