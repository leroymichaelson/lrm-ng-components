import { Component, OnInit } from '@angular/core';

export interface IIssue {
  name: string;
  desc: string;
}

export interface IFeature {
  name: string;
  issues?: IIssue[];
}

@Component({
  selector: 'tut-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent {

  features: IFeature[] = [];

  constructor() {

    this.features.push(
      {
        name: 'Popover',
        issues: [
          {
            name: 'Component Based ???',
            desc: 'Allow for both components and templates',
          },
        ]
      }
    );

    this.features.push(
      {
        name: 'Password',
        issues: [
          {
            name: 'Constraints',
            desc: 'Are there any constraints (e.g., min/max length, pattern)?',
          },
        ]
      }
    );
  }
}
