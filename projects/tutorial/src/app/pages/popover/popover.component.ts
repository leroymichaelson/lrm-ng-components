import {AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {PopoverService} from '../../../../../components/src/lib/popover/popover.service';
import {PopoverRef} from '../../../../../components/src/lib/popover/popover-ref';

@Component({
  selector: 'tut-popup',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
  providers: [PopoverService]
})
export class PopoverComponent {

  constructor(private popoverService: PopoverService) { }

  private popoverRef: PopoverRef<any>;

  show(element: HTMLElement, template: TemplateRef<any>) {

    this.popoverRef = this.popoverService.open({
      origin: element,
      content: template,
    });
  }

  showFixedWidth(element: HTMLElement, template: TemplateRef<any>) {

    this.popoverRef = this.popoverService.open({
      origin: element,
      content: template,
      width: '200px'
    });
  }

  showFixedHeight(element: HTMLElement, template: TemplateRef<any>) {

    this.popoverRef = this.popoverService.open({
      origin: element,
      content: template,
      height: '300px'
    });
  }

  close() {
    if ( this.popoverRef ) {
      this.popoverRef.close();
    }
  }
}
