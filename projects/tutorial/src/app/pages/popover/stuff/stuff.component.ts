import {Component, Injectable, Injector, OnInit} from '@angular/core';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';


@Component({
  selector: 'tut-stuff',
  templateUrl: './stuff.component.html',
  styleUrls: ['./stuff.component.scss']

})
export class StuffComponent implements OnInit {

  ngOnInit() {
  }

}
