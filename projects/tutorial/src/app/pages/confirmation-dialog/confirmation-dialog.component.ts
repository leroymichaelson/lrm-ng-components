import { Component, OnInit } from '@angular/core';
import {ParametersService} from '../../parameters/parameters.service';

@Component({
  selector: 'tut-modals',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
//  providers: [MessageService]
})
export class ConfirmationDialogComponent implements OnInit {

  displayBiDialog = false;
  displayPrimeDialog = false;
  displaySimpleWarningDialog = false;
  displayWarningDialog = false;
  displayErrorDialog = false;
  displaySingleDialog = false;
  displayWidthDialog = false;

  constructor(
    public parameters: ParametersService,
//    private messageService: MessageService
  ) { }

  ngOnInit() {
  }

  showBiDialog() {
    this.displayBiDialog = true;
  }

  showPrimeDialog() {
    this.displayPrimeDialog = true;
  }

  onYes() {
//    this.messageService.add({severity: 'success', summary: 'Yes', detail: 'Positive answer was chosen'});
  }

  onNo() {
//    this.messageService.add({severity: 'error', summary: 'No', detail: 'Negative answer was chosen'});
  }
}
