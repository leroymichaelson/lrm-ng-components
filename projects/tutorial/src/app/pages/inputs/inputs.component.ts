import { Component } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ParametersService} from '../../parameters/parameters.service';

@Component({
  selector: 'tut-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent {

  form = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    paragraph: new FormControl('The quick brown fox jumps over the lazy dog.'),
    sendWelcomeEmail: new FormControl(true),
    email: new FormControl(''),
    date: new FormControl(''),
    phone: new FormControl(''),
    phoneFilled: new FormControl('+310649785309'),
    deliveryPhone: new FormControl(''),
    deliveryPhoneFilled: new FormControl('+91802331323'),
    ssn: new FormControl(''),
    time: new FormControl(null),
    dateTime: new FormControl(''),
    dateTimeFilled: new FormControl(new Date(2022, 5, 10, 10, 33, 12))
  });

  constructor(public parameters: ParametersService) { }
}
