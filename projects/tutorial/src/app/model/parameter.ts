interface IParameter {
  name: string;
  type: string;
  default?: string;
  desc?: string;
}
