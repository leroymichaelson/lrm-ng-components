import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SampleFormComponent } from './pages/sample-form/sample-form.component';
import { FormSamplerComponent } from './pages/form-sampler/form-sampler.component';
import { RequiredIndicatorComponent } from './pages/required-indicator/required-indicator.component';
import { PageComponent } from './pages/layout/page/page.component';
import { Layout01Component } from './pages/layout/layout01/layout01.component';
import { Layout02Component } from './pages/layout/layout02/layout02.component';
import { ConfirmationDialogComponent } from './pages/confirmation-dialog/confirmation-dialog.component';
import { LoadingIndicatorComponent } from './pages/loading-indicator/loading-indicator.component';
import { InstallationComponent } from './pages/installation/installation.component';
import { RoadmapComponent } from './pages/roadmap/roadmap.component';
import { ReleaseNotesComponent } from './pages/release-notes/release-notes.component';
import { HomeComponent } from './pages/home/home.component';
import { IndexComponent } from './pages/index/index.component';
import { ActionsComponent } from './pages/actions/actions.component';
import { InputsComponent } from './pages/inputs/inputs.component';
import { SelectionsComponent } from './pages/selections/selections.component';
import { DevelopmentComponent } from './pages/development/development.component';
import { IssuesComponent } from './pages/issues/issues.component';
import { ActionContainerComponent } from './pages/action-container/action-container.component';
import { DialogComponent } from './pages/dialog/dialog.component';
import { PageWithToolsComponent } from './pages/layout/page-with-tools/page-with-tools.component';
import { PopoverComponent } from './pages/popover/popover.component';
import { OutputsComponent } from './pages/outputs/outputs.component';
import {TabbedCardComponent} from './pages/layout/tabbed-card/tabbed-card.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'actioncontainer', component: ActionContainerComponent },
  { path: 'actions', component: ActionsComponent },
  { path: 'confirmationdialog', component: ConfirmationDialogComponent },
  { path: 'development', component: DevelopmentComponent },
  { path: 'dialog', component: DialogComponent },
  { path: 'formsampler', component: FormSamplerComponent },
  { path: 'index', component: IndexComponent },
  { path: 'inputs', component: InputsComponent },
  { path: 'installation', component: InstallationComponent },
  { path: 'issues', component: IssuesComponent },
  { path: 'layout01', component: Layout01Component },
  { path: 'layout02', component: Layout02Component },
  { path: 'loadingindicator', component: LoadingIndicatorComponent },
  { path: 'page', component: PageComponent },
  { path: 'pagewithtools', component: PageWithToolsComponent },
  { path: 'popover', component: PopoverComponent },
  { path: 'releasenotes', component: ReleaseNotesComponent },
  { path: 'requiredindicator', component: RequiredIndicatorComponent },
  { path: 'roadmap', component: RoadmapComponent },
  { path: 'sampleform', component: SampleFormComponent },
  { path: 'selections', component: SelectionsComponent },
  { path: 'tabbedcard', component: TabbedCardComponent },
  { path: 'outputs', component: OutputsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
