import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lrm-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  @Input() name: string;
  @Input() icon: string;

  constructor() { }

  ngOnInit() { }
}
