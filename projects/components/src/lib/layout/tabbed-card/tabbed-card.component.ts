import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  QueryList, TemplateRef,
  ViewChild,
} from '@angular/core';
import {TabComponent} from '../tab/tab.component';
import {PopoverRef} from '../../popover/popover-ref';
import {PopoverService} from '../../popover/popover.service';

// TODO The menu type used for the public interface.
// TODO This may not be necessary anymore
export interface IBriefMenu {
  text: string;
}

/**
 * Used for internal tracking
 */
interface ITrackingMenuItem extends IBriefMenu {
  width?: number;
  visible?: boolean;
  selected?: boolean;
  tab?: TabComponent;
}

@Component({
  selector: 'lrm-tabbed-card',
  templateUrl: './tabbed-card.component.html',
  styleUrls: ['./tabbed-card.component.scss'],
  providers: [PopoverService]
})
export class TabbedCardComponent implements OnInit, AfterViewInit {

  @Input() scrollable = true;

  items: ITrackingMenuItem[];
  secondaryItemsExist = false;
  private popoverRef: PopoverRef<any>;

  /**
   * The list of tabs that the user has defined with <lrm-tab>
   */
  @ContentChildren(TabComponent) tablist: QueryList<TabComponent>;

  /**
   * The hidden version of tabs used to measure the width of the text strings
   */
  @ViewChild('hiddenTabs', {static: true}) hiddenItems: ElementRef;

  constructor(
    private element: ElementRef,
    private popoverService: PopoverService
  ) { }

  ngOnInit() {
  }

  // TODO Might have to listen to the host DOM size change, if possible.
  // Or, have the owner of the paged card call computeLayout when necessary.

  /**
   * This assumes that the tabs are defined statically.  If they are defined dynamically, we probably
   * have to call doLayout() after the tabs are created (like if they are created from an async call.
   *
   * Or, we could just make the tabs definition model driven instead of markup driven.
   */
  ngAfterViewInit(): void {
    this.doLayout();
  }

  private doLayout() {
    this.initializeTabs();
    setTimeout(() => {
      this.measureItems();
      this.computeLayoutWithDelay();
      this.itemSelected( this.items[0]);
    }, 0);
  }

  private showTabContent(item: ITrackingMenuItem) {
    this.items.forEach((i) => {
      i.tab.visible = false;
    });
    item.tab.visible = true;
  }

  itemSelected(item: ITrackingMenuItem) {
    this.items.forEach((i) => {
      i.selected = false;
    });
    item.selected = true;
    this.showTabContent(item);
    if ( this.popoverRef ) {
      this.popoverRef.close();
    }
  }

  /**
   * Accomodate animations that happen.
   * This makes for a smoother experience when stretching the page.
   */
  private computeLayoutWithDelay() {
    for ( let i = 100; i < 600; i += 50 ) { // Milliseconds
      setTimeout( () => {
        this.computeLayout();
      }, i );
    }
  }

  @HostListener('window:resize')
  handleWindowResize() {
    this.computeLayout();
  }

  private computeLayout() {
    const availableWidth = this.element.nativeElement.clientWidth - 60;
    let remainingWidth = availableWidth - 110;  // The 100 could be computed.  It is just arrived at emperically.
    this.secondaryItemsExist = false;
    this.items.forEach( (item) => {
      if ( item.width <= remainingWidth ) {
        item.visible = true;
        remainingWidth -= item.width;
      } else {
        item.visible = false;
        this.secondaryItemsExist = true;
      }
    });
  }

  /**
   * Measure the width of each text string
   */
  private measureItems() {
    const rawTabs: any[] = this.hiddenItems.nativeElement.children;

    for ( let i = 0; i < rawTabs.length; i++ ) {
        const item = rawTabs[i];
        this.items[i].width = this.getItemTextWidth(item);
    }
  }

  /**
   * @param item The nativeElement of one of the tabs
   * @return The width of the text in the tab element
   */
  private getItemTextWidth( item: HTMLElement ): number {
    const span: any = item.childNodes[0]; // Presumed to be the only child
    const rectangle = span.getBoundingClientRect();
    const width = rectangle.width;
    return width;
  }

  private initializeTabs() {

    this.items = [];
    this.tablist.forEach((tab) => {
      this.items.push({
          text: tab.label,
          tab,
        });
    });

    this.items.forEach( (item) => {
      item.visible = true;
      item.selected = false;
    });

    this.items[0].tab.visible = true;
  }

  showSecondaryMenu(element: HTMLElement, template: TemplateRef<any>) {
    this.popoverRef = this.popoverService.open({
      origin: element,
      content: template,
      positions: PopoverService.getBottomPositions(),
    });

  }
}
