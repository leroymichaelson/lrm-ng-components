import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lrm-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() scrollable = true;

  constructor() { }

  ngOnInit() {
  }

}
