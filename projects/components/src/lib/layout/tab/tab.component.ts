import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lrm-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() label = '';

  @Input() visible = false;

  constructor() { }

  ngOnInit() {
  }

}
