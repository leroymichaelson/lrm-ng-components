import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'lrm-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit, AfterViewInit {

  private wasManuallyClosed = false;

  @ViewChild('defaultHeader', {static: true}) defaultHeader: ElementRef;
  @ViewChild('positiveAnswer', {static: true}) defaultPositiveAnswer: ElementRef;
  @ViewChild('negativeAnswer', {static: true}) defaultNegativeAnswer: ElementRef;

  @Input() header: string;
  @Input() positiveText: string;
  @Input() negativeText: string;

  @Input() set severity(value: string) {
    this.styleClass = value;
  }

  @Input() visible = false;

  @Input() showPositive = true;
  @Input() showNegative = true;
  @Input() style;

  @Output() clickPositive: EventEmitter<any> = new EventEmitter();
  @Output() clickNegative: EventEmitter<any> = new EventEmitter();

  styleClass = 'warning';

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.populateDefaultValues();
    }, 0);
  }

  private populateDefaultValues() {
    if (!this.header) {
      this.header = this.defaultHeader.nativeElement.textContent;
    }
    if (!this.positiveText ) {
      this.positiveText = this.defaultPositiveAnswer.nativeElement.textContent;
    }
    if (!this.negativeText) {
      this.negativeText = this.defaultNegativeAnswer.nativeElement.textContent;
    }
  }

  clickPositiveHandler() {
    this.wasManuallyClosed = true;
    this.visible = false;
    this.clickPositive.emit();
  }

  clickNegativeHandler() {
    this.wasManuallyClosed = true;
    this.visible = false;
    this.closeNegative();
  }

  private closeNegative() {
    this.clickNegative.emit();
  }

  checkForNonbuttonClose() {
    if (!this.wasManuallyClosed) {
      this.closeNegative();
    }
    this.wasManuallyClosed = false;
  }
}
