import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelValueListComponent } from './label-value-list.component';

describe('LabelValueListComponent', () => {
  let component: LabelValueListComponent;
  let fixture: ComponentFixture<LabelValueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabelValueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelValueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
