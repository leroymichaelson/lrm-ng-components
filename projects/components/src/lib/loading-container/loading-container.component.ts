import { Component, Input, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'lrm-loading-container',
  templateUrl: './loading-container.component.html',
  styleUrls: ['./loading-container.component.scss']
})
export class LoadingContainerComponent implements OnInit {
  @Input() isLoading = false;
  @Input() hasError = false;
  @Input() showContentWhileLoading = false;

  constructor() { }

  ngOnInit() {
  }
}
