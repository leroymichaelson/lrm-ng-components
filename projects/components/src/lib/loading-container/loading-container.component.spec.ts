import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoadingContainerComponent } from './loading-container.component';
import { LoadingIndicatorComponent } from '../loading-indicator/loading-indicator.component';
import { ErrorIndicatorComponent } from '../error-indicator/error-indicator.component';

describe('LoadingContainerComponent', () => {
  let component: LoadingContainerComponent;
  let fixture: ComponentFixture<LoadingContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoadingContainerComponent,
        LoadingIndicatorComponent,
        ErrorIndicatorComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingContainerComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
