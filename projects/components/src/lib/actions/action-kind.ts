export enum Kind {
  Save,
  Primary,
  Secondary,
  Tertiary
}
