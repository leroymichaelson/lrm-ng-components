import {ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Kind} from './action-kind';
import {Subject} from 'rxjs';
import {Buttons} from './action-buttons';

export abstract class BaseActionComponent implements OnInit {
  buttonset = Buttons; // The enum instance

  @Input() public disabled = false;
  @Input() type = 'button';

  /**
   * Allow the instance to override the visual type.  This should be used
   * int rare instances. Most often it will be changed when two primary buttons
   * are used in a context.  One of them would be changed to secondary.
   *
   * @param value  'primary' | 'secondary' | 'tertiary'
   */
  @Input() set kind(value: string) {
    switch (value) {
      case 'primary':
        this.setStyle(Kind.Primary);
        break;
      case 'secondary':
        this.setStyle(Kind.Secondary);
        break;
      case 'tertiary':
        this.setStyle(Kind.Tertiary);
        break;
    }
  }

  /**
   * Options are:
   *   - default
   *   - "small" - for embedded buttons
   */
  @Input() size = 'normal';

  @Output() execute: EventEmitter<any> = new EventEmitter();

  text$ = new Subject<string>();

  styleClass: string;

  constructor(private rootElement: ElementRef, kind: Kind, public button: Buttons, public icon: string = null) {
    this.setStyle(kind);
  }

  private setStyle(kind: Kind) {
    switch (kind) {
      case Kind.Save:
        this.styleClass = 'persist';
        break;
      case Kind.Primary:
        this.styleClass = 'primary';
        break;
      case Kind.Secondary:
        this.styleClass = 'secondary';
        break;
      case Kind.Tertiary:
        this.styleClass = 'tertiary';
        break;
    }
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.findAndSetText();
    }, 0);
  }

  private findAndSetText() {
    const ne = this.rootElement.nativeElement;
    const elements = ne.querySelectorAll('.lrm-translation');
    this.text$.next(elements[0].innerText);
  }
}

