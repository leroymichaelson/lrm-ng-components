import {Component, ElementRef} from '@angular/core';
import {Kind} from './action-kind';
import {BaseActionComponent} from './action-base';
import {Buttons} from './action-buttons';

@Component({
  selector: 'lrm-action-export',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionExportComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Export);
  }
}

