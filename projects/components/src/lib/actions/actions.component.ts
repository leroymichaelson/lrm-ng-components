import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Subject } from 'rxjs';
import {Kind} from './action-kind';
import {BaseActionComponent} from './action-base';
import {Buttons} from './action-buttons';

@Component({
  selector: 'lrm-action-add',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionAddComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Add);
  }
}

@Component({
  selector: 'lrm-action-back',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionBackComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Back);
  }
}

@Component({
  selector: 'lrm-action-cancel',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionCancelComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Tertiary, Buttons.Cancel, 'fas fa-times');
  }
}

@Component({
  selector: 'lrm-action-close',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionCloseComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Close, 'fas fa-times');
  }
}

@Component({
  selector: 'lrm-action-complete',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionCompleteComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Complete);
  }
}

@Component({
  selector: 'lrm-action-continue',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionContinueComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Continue);
  }
}

@Component({
  selector: 'lrm-action-create',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionCreateComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Create);
  }
}

@Component({
  selector: 'lrm-action-delete',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionDeleteComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Secondary, Buttons.Delete, 'fas fa-trash');
  }
}

@Component({
  selector: 'lrm-action-deny',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionDenyComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Deny);
  }
}

@Component({
  selector: 'lrm-action-decline',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionDeclineComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Secondary, Buttons.Decline);
  }
}

@Component({
  selector: 'lrm-action-download',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionDownloadComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Download);
  }
}

@Component({
  selector: 'lrm-action-edit',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionEditComponent extends BaseActionComponent {

  constructor(el: ElementRef) {
    super( el, Kind.Primary, Buttons.Edit, 'fas fa-edit' );
  }
}

@Component({
  selector: 'lrm-action-go',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionGoComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Go);
  }
}

@Component({
  selector: 'lrm-action-logout',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionLogoutComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Logout);
  }
}

@Component({
  selector: 'lrm-action-next',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionNextComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Next, 'fas fa-chevron-right');
  }
}

@Component({
  selector: 'lrm-action-no',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionNoComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Tertiary, Buttons.No);
  }
}

@Component({
  selector: 'lrm-action-previous',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionPreviousComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Previous, 'fas fa-chevron-left' );
  }
}

@Component({
  selector: 'lrm-action-print',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionPrintComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Print );
  }
}

@Component({
  selector: 'lrm-action-refresh',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionRefreshComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Refresh);
  }
}

@Component({
  selector: 'lrm-action-remove',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionRemoveComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Secondary, Buttons.Remove);
  }
}

@Component({
  selector: 'lrm-action-reset',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionResetComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Reset);
    this.type = 'reset';
  }
}

@Component({
  selector: 'lrm-action-save',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionSaveComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Save, Buttons.Save, 'fas fa-save');
  }
}

@Component({
  selector: 'lrm-action-search',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionSearchComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Search);
  }
}

@Component({
  selector: 'lrm-action-select',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionSelectComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Select);
  }
}

@Component({
  selector: 'lrm-action-submit',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionSubmitComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Submit);
    this.type = 'submit';
  }
}

@Component({
  selector: 'lrm-action-upload',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionUploadComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Upload);
  }
}

@Component({
  selector: 'lrm-action-wait',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionWaitComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Wait);
  }
}

@Component({
  selector: 'lrm-action-yes',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionYesComponent extends BaseActionComponent {
  constructor(el: ElementRef) {
    super(el, Kind.Primary, Buttons.Yes);
  }
}
