import { Component, OnInit } from '@angular/core';
import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class MinLengthValidator {

  maxLength: number;

  validator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if ( !this.maxLength ) {
        const length: number = control.value ? control.value.length : 0;
        if ( length > this.maxLength ) {
          return {
            maxlength: {
              requiredLength: this.maxLength,
              actualLength: length }
          };
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }
}
