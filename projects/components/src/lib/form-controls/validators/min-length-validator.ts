import { Component, OnInit } from '@angular/core';
import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class MinLengthValidator {

  minLength: number;

  validator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if ( !this.minLength ) {
        const length: number = control.value ? control.value.length : 0;
        if ( length < this.minLength ) {
          return {
            minlength: {
              requiredLength: this.minLength,
              actualLength: length }
          };
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }
}
