import { AfterViewInit, ElementRef, Injector, Input, OnInit, Type, OnDestroy } from '@angular/core';
import { FormControl, NgControl, ValidatorFn } from '@angular/forms';
import { BaseControlValueAccessor } from './inputs/base-control-value-accessor';
import { LRMErrorStateMatcher } from './inputs/error-state-matcher';
import { Subscription } from 'rxjs';

export abstract class InputBase<T> extends BaseControlValueAccessor<T>
  implements OnInit, OnDestroy, AfterViewInit {

  constructor(protected injector: Injector, protected element: ElementRef) {
    super();
  }

  @Input() required = false;
  @Input() disabled = false;
  @Input() label: any;
  @Input() hint = '';

  formControl: FormControl;
  private ngControl: NgControl;

  private subscriptions: Subscription[] = [];

  /**
   * The error name that will be displayed (not the error text)
   */
  private mostImportantError: string;
  private initialValidators: ValidatorFn;

  errorStateMatcher = new LRMErrorStateMatcher();

  setNgControl( ngControl: NgControl ) {
    this.ngControl = ngControl;
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  /**
   * Override to add more validators
   */
  protected validators(): ValidatorFn[] {
    return [];
  }

  /**
   * Returns a list of the errors of interest to the component, in priority order.
   *
   * Override to add a list of errors
   */
  protected errorsOfInterest(): string[] {
    return ['required'];
  }

  /**
   * Subclasses can override this when they need to act on a value change.
   */
  protected valueChanged() {}

  ngAfterViewInit(): void {
    setTimeout(() => {

      if (!this.ngControl || !this.ngControl.control) {
        console.warn(
          'InputBase.ngAfterViewInit() Form Control is missing.' +
            ' Maybe this control is not part of a form, or the formControlName attribute is missing '
        );

        return;
      }

      this.formControl = this.ngControl.control as FormControl;
      this.initialValidators = this.formControl.validator;

      if (this.element) {
        this.element.nativeElement.classList.add('lrm-input');
      }

      this.subscriptions.push(
        this.formControl.valueChanges.subscribe(() => {
          this.chooseMostImportantError();
          this.configureStyles();
          this.valueChanged();
        })
      );

      this.formControl.registerOnDisabledChange(() => {});
      this.resetValidators();
    }, 0);
  }

  protected resetValidators() {
    if (!this.formControl) {
      return;
    }

    const validators: ValidatorFn[] = [];
    const newValidators = this.validators();

    if (this.initialValidators) {
      validators.push(this.initialValidators);
    }

    if (newValidators) {
      newValidators.forEach((validator) => {
        validators.push(validator);
      });
    }

    this.formControl.setValidators(validators);
    this.formControl.updateValueAndValidity();
  }

  private testError(name: string) {
    if (this.formControl.hasError(name)) {
      this.mostImportantError = name;
    }
  }

  public configureStyles() {
    if (this.formControl.disabled) {
      this.element.nativeElement.classList.add('disabled');
    } else {
      this.element.nativeElement.classList.remove('disabled');
    }

    if (this.formControl.valid) {
      this.element.nativeElement.classList.remove('form-error');
    } else {
      this.element.nativeElement.classList.add('form-error');
    }
  }

  private chooseMostImportantError() {
    this.mostImportantError = '';
    const errors = this.errorsOfInterest();

    for (let i = errors.length; i > 0; i--) {
      this.testError(errors[i - 1]);
    }
  }

  /**
   * Returns true if the named error exists at the moment.
   */
  public hasError(name: string) {
    if (!this.mostImportantError) {
      return false;
    } else {
      return this.mostImportantError === name;
    }
  }

  allowOnlyNumberKeys(event: { charCode: number; }) {
    return event.charCode === 8 || event.charCode === 0
      ? null
      : event.charCode >= 48 && event.charCode <= 57;
  }
}
