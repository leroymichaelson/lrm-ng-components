import { AfterViewInit, Component, ElementRef, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lrm-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss']
})
export class OptionComponent implements OnInit, AfterViewInit {

  /*
   I am thinking that we should use getters and setters here.
   If the value is null, then return name as the value.
   */
  // tslint:disable-next-line:variable-name
  private _value: any;
  @Input()
  set value(val) {
    this._value = val;
  }
  get value() {
    return this._value;
  }

  name: string; // Derived from transclusion

  constructor(private el: ElementRef) { }

  /*
   * computeNameValue needs to be computed twice.  First, to make sure the value
   * is available early. Second, to make sure that name is fetched when the information
   * is supplied async.
   */
  ngOnInit() {
    this.computeNameValue();
  }

  ngAfterViewInit(): void {
    this.computeNameValue();
  }

  private computeNameValue() {
//    this.name = this.el.nativeElement.innerText;
    this.name = this.el.nativeElement.innerHTML;
    if ( !this.value ) {
      this.value = this.name;
    }
  }
}
