import { AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit, ViewChild } from '@angular/core';
import { InputBase } from '../../input-base';
import {NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';

@Component({
  selector: 'lrm-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDateComponent),
      multi: true,
    },
  ],
})
export class InputDateComponent extends InputBase<Date>
  implements OnInit, AfterViewInit {
  @ViewChild('defaultLabel', { static: true })
  defaultLabel: ElementRef;

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngOnInit() {
    super.ngOnInit();

    if (!this.label) {
      this.label = this.defaultLabel.nativeElement.innerText;
    }
  }

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }
}
