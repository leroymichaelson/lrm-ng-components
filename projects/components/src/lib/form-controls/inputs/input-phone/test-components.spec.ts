import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  template: `<form [formGroup]="form">
      <lrm-input-phone formControlName="phone"></lrm-input-phone>
    </form>`
})
export class SimpleTestComponent {
  form = new FormGroup({
    phone: new FormControl('')
  });
}

@Component({
  template: `<form [formGroup]="form">
      <lrm-input-phone formControlName="phone" country="gb"></lrm-input-phone>
    </form>`
})
export class CountrySetTestComponent {
  form = new FormGroup({
    phone: new FormControl('')
  });
}

@Component({
  template: `<form [formGroup]="form">
      <lrm-input-phone formControlName="phone" country="it"></lrm-input-phone>
    </form>`
})
export class ValueSetTestComponent {
  form = new FormGroup({
    phone: new FormControl('+31 0649785309')
  });
}

@Component({
  template: `<form [formGroup]="form">
      <lrm-input-phone formControlName="phone"></lrm-input-phone>
    </form>`
})
export class AmericanSamoaTestComponent {
  form = new FormGroup({
    phone: new FormControl('+1684 1234567')
  });
}
