import { async, TestBed } from '@angular/core/testing';
import { InputPhoneComponent } from './input-phone.component';
import { MatFormFieldModule, MatInputModule, MatIconModule, MatSelectModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleTestComponent, CountrySetTestComponent, ValueSetTestComponent, AmericanSamoaTestComponent } from './test-components.spec';

describe('InputPhoneComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InputPhoneComponent,
        SimpleTestComponent,
        CountrySetTestComponent,
        ValueSetTestComponent,
        AmericanSamoaTestComponent
      ],
      imports: [
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatSelectModule,
        TextMaskModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  it('should create input phone component', () => {
    // given / test
    const testFixture = TestBed.createComponent(InputPhoneComponent);
    const inputPhoneComponent = testFixture.componentInstance;

    // expect
    expect(inputPhoneComponent).toBeTruthy();
  });

  it('should create simple test component', () => {
    // given / test
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    const testComponent = testFixture.componentInstance;

    // expect
    expect(testComponent).toBeTruthy();
  });

  it('should display default country in selection, when not configured', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);

    // test
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();

      const selectedCountry = testFixture.debugElement
        .query(By.css('lrm-input-phone[ng-reflect-name="phone"] .flag-container'))
        .nativeElement.getAttribute('title');

      // expect
      expect(selectedCountry).toBe('United States +1');
    });
  }));

  it('should display configured country in selection, when configured', async(() => {
    // given
    const testFixture = TestBed.createComponent(CountrySetTestComponent);

    // test
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();

      const selectedCountry = testFixture.debugElement
        .query(By.css('lrm-input-phone[ng-reflect-name="phone"] .flag-container'))
        .nativeElement.getAttribute('title');

      // expect
      expect(selectedCountry).toBe('United Kingdom +44');
    });
  }));

  it('should override configured country in selection, when phone number is set', async(() => {
    // given
    const testFixture = TestBed.createComponent(CountrySetTestComponent);
    const component = testFixture.componentInstance;
    component.form.get('phone').setValue('+31 0201234567');

    // test
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();

      const selectedCountry = testFixture.debugElement
        .query(By.css('lrm-input-phone[ng-reflect-name="phone"] .flag-container'))
        .nativeElement.getAttribute('title');

      // expect
      expect(selectedCountry).toBe('Netherlands +31');
    });
  }));

  it('should display correct selected country, when phone number is set', async(() => {
    // given
    const testFixture = TestBed.createComponent(AmericanSamoaTestComponent);

    // test
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();

      const selectedCountry = testFixture.debugElement
        .query(By.css('lrm-input-phone[ng-reflect-name="phone"] .flag-container'))
        .nativeElement.getAttribute('title');

      // expect
      expect(selectedCountry).toBe('American Samoa +1684');
    });
  }));

  it('should change country code of phone number when selection changes', async(() => {
    // given
    const testFixture = TestBed.createComponent(ValueSetTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputPhoneComponent = testFixture.debugElement.query(By.css('lrm-input-phone')).componentInstance;

      // test
      inputPhoneComponent.selectedCountry = {
        name: 'American Samoa',
        iso2Code: 'as',
        dialCode: '1684',
        phoneMask: [],
        phonePattern: '',
        order: 0
      };

      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        setTimeout(() => {
          const inputElement = testFixture.debugElement.query(By.css('lrm-input-phone input')).nativeElement;
          expect(inputElement.value).toBe('+1684 0649785309');
        });
      });
    });
  }));

  it('should set initial value on focus when input value is empty', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputPhoneComponent = testFixture.debugElement.query(By.css('lrm-input-phone')).componentInstance;

      // test
      inputPhoneComponent.onFocus();

      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        setTimeout(() => {
          const inputElement = testFixture.debugElement.query(By.css('lrm-input-phone input')).nativeElement;
          expect(inputElement.value).toBe('+1 (');
        });
      });
    });
  }));

  it('should empty value on blur when input value has only country code set', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputPhoneComponent = testFixture.debugElement.query(By.css('lrm-input-phone')).componentInstance;

      // test
      inputPhoneComponent.onBlur();

      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        setTimeout(() => {
          const inputElement = testFixture.debugElement.query(By.css('lrm-input-phone input')).nativeElement;
          expect(inputElement.value).toBe('');
        });
      });
    });
  }));

  it('should set filtered country list when searching part of name', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputPhoneComponent = testFixture.debugElement.query(By.css('lrm-input-phone')).componentInstance;

      // test
      inputPhoneComponent.onSearch('ne');

      // expect
      expect(inputPhoneComponent.countries.length).toBe(5);

      inputPhoneComponent.countries.forEach(x => {
        const nameValue = x.name.substring(0, 2).toLowerCase();
        const iso2Code = x.iso2Code.toLowerCase();
        const valueToCheck = (nameValue === 'ne' ? nameValue : iso2Code);
        expect(valueToCheck).toBe('ne');
      });
    });
  }));
});
