/**
 * Country model:
 * [
 *    Country name,
 *    Iso2 code,
 *    International dial code,
 *    Phone mask (if available),
 *    Phone pattern (if available)
 * ]
 */
export interface ICountry {
  name: string;
  iso2Code: string;
  dialCode: string;
  phoneMask: any[];
  phonePattern: string;
  order: number;
}
