import { Component, ElementRef, forwardRef, Injector, ViewChild, Input, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import {NG_VALUE_ACCESSOR, ValidatorFn, Validators, AbstractControl, NgControl} from '@angular/forms';
import { InputTextComponent } from '../input-text/input-text.component';
import { PhoneService } from '../../../services/phone.service';
import { ICountry } from './country';

@Component({
  selector: 'lrm-input-phone',
  templateUrl: './input-phone.component.html',
  styleUrls: ['./input-phone.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputPhoneComponent),
    multi: true
  }]
})
export class InputPhoneComponent extends InputTextComponent implements OnInit, AfterViewChecked {
  private defaultCountry = 'us';

  @Input()
  public country: string = this.defaultCountry;

  @ViewChild('defaultLabel', { static: true })
  defaultLabel: ElementRef;

  @ViewChild('countrySelect', null)
  countrySelect: any;

  @ViewChild('phoneInput', null)
  phoneInput: any;

  @ViewChild('searchInput', null)
  searchInput: any;

  constructor(
    injector: Injector,
    element: ElementRef,
    private changeDetector: ChangeDetectorRef,
    private phoneService: PhoneService
  ) {
    super(injector, element);
  }

  public inputMask: any[];
  public countries: ICountry[];

  private inputCountry: ICountry;
  private defaultMaxLength = 22;
  private defaultPattern = '^\\+[0-9_\\s\\-\\(\\)]+$';
  private phonePattern: string;
  private minLength = 0;
  private maxLength = 0;
  private hasFocus = false;

  get selectedCountry(): ICountry {
    return this.inputCountry;
  }
  set selectedCountry(value: ICountry) {
    const previousCountry = this.inputCountry;
    this.inputCountry = value;
    this.selectedCountryChanged(previousCountry);
  }

  get countryTitle(): string {
    return !this.inputCountry ? '' : `${this.inputCountry.name} +${this.inputCountry.dialCode}`;
  }

  get countryIso2Code(): string {
    return !this.inputCountry ? this.defaultCountry : this.inputCountry.iso2Code;
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.country = this.country || this.defaultCountry;

    if (!this.label) {
      this.label = this.defaultLabel.nativeElement.innerText;
    }
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }

  public valueChanged() {
    if (!this.formControl) {
      return;
    }

    if (!this.hasFocus && !this.inputCountry) {
      const formControlDialCode = this.extractIso2CodeFromFormValue(this.formControl.value);

      this.setInputCountry(formControlDialCode
        ? `+${this.phoneService.getCountry(formControlDialCode, this.country).dialCode} `
        : `+${this.phoneService.getCountry(this.country, this.country).dialCode} `);

      return;
    }

    let valueToCheck: string;

    if (this.formControl.value.length > 0 || this.inputCountry) {
      valueToCheck = `+${this.formControl.value.replace(/\D+/g, '')}`;
      const dialCode = this.inputCountry ? this.inputCountry.dialCode : '';

      if (dialCode.length > 0 && valueToCheck.length > `+${dialCode}`.length) {
        valueToCheck = valueToCheck.replace(`+${dialCode}`, `+${dialCode} `);
      }
    } else {
      valueToCheck = `+${this.phoneService.getCountry(this.country, this.defaultCountry).dialCode} `;
    }

    if ((!this.inputCountry || !valueToCheck.startsWith(`+${this.inputCountry.dialCode}`)) &&
      (valueToCheck && valueToCheck !== '+')) {
      this.setInputCountry(valueToCheck);
    }

    if (this.formControl.value !== valueToCheck && valueToCheck !== '+') {
      this.setFormControlValue(valueToCheck);
    }
  }

  public onBlur() {
    this.hasFocus = false;
    const dialCode = this.inputCountry ? this.inputCountry.dialCode : '';

    if (this.formControl.value.trim().length <= `+${dialCode}`.length) {
      this.setFormControlValue('');
    }
  }

  public onFocus() {
    this.hasFocus = true;

    const dialCode = this.inputCountry
      ? this.inputCountry.dialCode
      : this.phoneService.getCountry(this.country, this.defaultCountry).dialCode;

    if (this.formControl.value.length <= `+${dialCode} `.length) {
      this.setFormControlValue(`+${dialCode} `);
    }
  }

  public displayDropDown() {
    this.countries = this.phoneService.getAllCountries();
    this.countrySelect.open();

    setTimeout(() => {
      this.searchInput.nativeElement.focus();
    });
  }

  public openedChange(open: boolean) {
    if (!open) {
      this.phoneInput.nativeElement.focus();
      this.onFocus();
      this.searchInput.nativeElement.value = '';
    }
  }

  public onSearch(phrase: string) {
    this.countries = this.phoneService.searchCountry(phrase);
  }

  protected validators(): ValidatorFn[] {
    return [
      Validators.minLength(this.minLength),
      Validators.maxLength(this.maxLength),
      Validators.pattern(this.phonePattern)
    ];
  }

  protected errorsOfInterest(): string[] {
    return super.errorsOfInterest();
  }

  private selectedCountryChanged(previousCountry: ICountry) {
    let formValue = this.formControl.value.replace(/\D+/g, '');

    formValue = '+' + ((formValue.startsWith(previousCountry.dialCode))
      ? this.inputCountry.dialCode + ' ' + formValue.substring(previousCountry.dialCode.length)
      : this.inputCountry.dialCode + ' ');

    this.inputMask = this.inputCountry.phoneMask;
    this.setFormControlValue(formValue);
    this.resetPropertiesAndValidators();
  }

  private setFormControlValue(value: string) {
    setTimeout(() => {
      if (value !== this.formControl.value) {
        this.formControl.setValue(value);
      }
    });
  }

  private setInputCountry(formControlValue: string) {
    let iso2Code: string;

    if (formControlValue && formControlValue.length > 0) {
      iso2Code = this.extractIso2CodeFromFormValue(formControlValue);
    }

    if (!iso2Code || (this.inputCountry && iso2Code === this.inputCountry.iso2Code)) {
      return;
    }

    this.inputCountry = this.phoneService.getCountry(iso2Code, this.defaultCountry);
    this.inputMask = this.inputCountry.phoneMask;
    this.resetPropertiesAndValidators();
  }

  private resetPropertiesAndValidators() {
    setTimeout(() => {
      this.phonePattern = this.inputCountry.phonePattern
        ? this.inputCountry.phonePattern
        : this.defaultPattern;

      this.maxLength = this.inputCountry.phoneMask && this.inputCountry.phoneMask.length > 0
        ? this.inputCountry.phoneMask.length
        : this.defaultMaxLength;

      super.resetValidators();
    });
  }

  private extractIso2CodeFromFormValue(formValue: string): string {
    const country = this.phoneService.getCountryByPhoneValue(formValue);

    return country ? country.iso2Code : null;
  }
}
