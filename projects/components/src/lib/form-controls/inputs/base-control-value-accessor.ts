import { ControlValueAccessor } from '@angular/forms';

export class BaseControlValueAccessor<T> implements ControlValueAccessor {

  // tslint:disable-next-line:variable-name
  public _disabled = false;
  set disabled(value) {
    this._disabled = value;
  }
  get disabled() {
    return this._disabled;
  }

  // tslint:disable-next-line:variable-name
  private _value: T;
  // tslint:disable-next-line:variable-name
  private _onChange: any;
  // tslint:disable-next-line:variable-name
  private _onTouched: any;

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    if ( this._onChange ) {
      this._onChange(val);
    }
  }

  /**
   * To be called by a subclass when blur
   */
  touched() {
    if ( this._onTouched ) {
      this._onTouched();
    }
  }

  /**
   * Model -> View changes
   */
  public writeValue(newValue: T): void {
    this.value = newValue;
  }

  public registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
