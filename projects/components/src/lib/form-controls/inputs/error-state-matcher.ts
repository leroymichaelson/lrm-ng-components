import {ErrorStateMatcher} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm} from '@angular/forms';

export class LRMErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;

    const answer =  !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));

    /*
        console.log( 'MATCHER ' );
        console.log( '  .. invalid: ' + control.invalid );
        console.log( '  .. dirty  : ' + control.dirty );
        console.log( '  .. touched: ' + control.touched );
        console.log( '  .. answer: ' + answer );
    */

    return answer;
  }
}
