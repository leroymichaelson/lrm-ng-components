import { AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit, ViewChild } from '@angular/core';
import {NG_VALUE_ACCESSOR, NgControl, ValidatorFn, Validators} from '@angular/forms';
import { InputTextComponent } from '../input-text/input-text.component';

@Component({
  selector: 'lrm-input-password',
  templateUrl: './input-password.component.html',
  styleUrls: ['./input-password.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputPasswordComponent),
      multi: true
    }
  ]
})
export class InputPasswordComponent extends InputTextComponent implements AfterViewInit {

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  minLength = 8;
  maxLength = 15;

  @ViewChild('defaultLabel', { static: true }) defaultLabel: ElementRef;

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
    setTimeout(() => {
      if ( !this.label ) {
        this.label = this.defaultLabel.nativeElement.innerText;
      }
    }, 0 );
  }

  protected validators(): ValidatorFn[] {
    return [
      Validators.minLength(this.minLength),
      Validators.maxLength(this.maxLength)];
  }

  protected errorsOfInterest(): string[] {
    return super.errorsOfInterest();
  }
}
