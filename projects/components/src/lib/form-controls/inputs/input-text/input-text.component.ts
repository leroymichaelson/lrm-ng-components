import {AfterViewInit, Component, ElementRef, forwardRef, Injector, Input} from '@angular/core';
import {InputBase} from '../../input-base';
import {NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';

@Component({
  selector: 'lrm-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTextComponent),
      multi: true
    }
  ]
})
export class InputTextComponent extends InputBase<string> implements AfterViewInit {
  @Input() placeholder = '';
  @Input() patternError = 'Does not match pattern';

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }

  protected errorsOfInterest(): string[] {
    return ['required', 'minlength', 'maxlength', 'pattern'];
  }
}
