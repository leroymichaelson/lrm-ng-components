import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InputSSNComponent } from './input-ssn.component';
import { MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

describe('InputSSNComponent', () => {
  let component: InputSSNComponent;
  let fixture: ComponentFixture<InputSSNComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InputSSNComponent
      ],
      imports: [
        MatInputModule,
        ReactiveFormsModule,
        TextMaskModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSSNComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
