import { Component, ElementRef, forwardRef, Injector, OnInit, ViewChild } from '@angular/core';
import {NG_VALUE_ACCESSOR, NgControl, ValidatorFn, Validators} from '@angular/forms';
import { InputTextComponent } from '../input-text/input-text.component';

@Component({
  selector: 'lrm-input-ssn',
  templateUrl: './input-ssn.component.html',
  styleUrls: ['./input-ssn.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputSSNComponent),
      multi: true
    }
  ]
})
export class InputSSNComponent extends InputTextComponent implements OnInit {

  @ViewChild('defaultLabel', { static: true }) defaultLabel: ElementRef;

  inputMask = [ /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/ ];
  minLength = 11;
  maxLength = 11;

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngOnInit() {
    super.ngOnInit();
    if ( !this.label ) {
      this.label = this.defaultLabel.nativeElement.innerText;
    }
  }

  protected validators(): ValidatorFn[] {
    return [
      Validators.minLength(this.minLength),
      Validators.maxLength(this.maxLength)
    ];
  }

  protected errorsOfInterest(): string[] {
    return ['required', 'minlength', 'maxlength'];
  }
}
