import { async, TestBed } from '@angular/core/testing';
import { InputDateTimeComponent } from './input-datetime.component';
import { InputDateComponent } from '../input-date/input-date.component';
import { InputTimeComponent } from '../input-time/input-time.component';
import { FormGroup, FormControl, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  template: `<form [formGroup]="form">
      <lrm-input-datetime
        formControlName="dateTime"
        dateLabel="Test date"
        dateHint="Fill in test date"
        timeLabel="Test time"
        timeHint="Fill in test time"
        defaultTime="10:10"
        required
        disabled>
      </lrm-input-datetime>
    </form>`
})
export class SimpleTestComponent {
  form = new FormGroup({
    dateTime: new FormControl(new Date(2033, 2, 14, 17, 25, 39))
  });
}

describe('InputDateTimeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InputDateTimeComponent,
        InputDateComponent,
        InputTimeComponent,
        SimpleTestComponent
      ],
      imports: [
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  it('should create input date time component', () => {
    // given / test
    const testFixture = TestBed.createComponent(InputDateTimeComponent);
    const inputDateTimeComponent = testFixture.componentInstance;

    // expect
    expect(inputDateTimeComponent).toBeTruthy();
  });

  it('should create the test component', () => {
    // given / test
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    const testComponent = testFixture.componentInstance;

    // expect
    expect(testComponent).toBeTruthy();
  });

  it('should fill the date and time components with the date time value given', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputDateComponent = testFixture.debugElement.query(By.css('lrm-input-date')).componentInstance;
      const inputTimeComponent = testFixture.debugElement.query(By.css('lrm-input-time')).componentInstance;

      // test
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(inputDateComponent.value).toEqual(new Date(2033, 2, 14, 17, 25, 39));
        expect(inputTimeComponent.value).toEqual(new Date(2033, 2, 14, 17, 25, 39));
      });
    });
  }));

  it('should set the hints on the date input and the time input', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputDateComponent = testFixture.debugElement.query(By.css('lrm-input-date')).componentInstance;
      const inputTimeComponent = testFixture.debugElement.query(By.css('lrm-input-time')).componentInstance;

      // test
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(inputDateComponent.hint).toBe('Fill in test date');
        expect(inputTimeComponent.hint).toBe('Fill in test time');
      });
    });
  }));

  it('should set the labels on the date input and the time input', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputDateComponent = testFixture.debugElement.query(By.css('lrm-input-date')).componentInstance;
      const inputTimeComponent = testFixture.debugElement.query(By.css('lrm-input-time')).componentInstance;

      // test
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(inputDateComponent.label).toBe('Test date');
        expect(inputTimeComponent.label).toBe('Test time');
      });
    });
  }));

  it('should set the default time on the time input', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputTimeComponent = testFixture.debugElement.query(By.css('lrm-input-time')).componentInstance;

      // test
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(inputTimeComponent.defaultTimeString).toBe('10:10');
      });
    });
  }));

  it('should set required on the date input and the time on the time input', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputDateComponent = testFixture.debugElement.query(By.css('lrm-input-date')).componentInstance;
      const inputTimeComponent = testFixture.debugElement.query(By.css('lrm-input-time')).componentInstance;

      // test
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(inputDateComponent.required).toBe('');
        expect(inputTimeComponent.required).toBe('');
      });
    });
  }));

  it('should set disabled on the date input and the time on the time input', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();
      const inputDateComponent = testFixture.debugElement.query(By.css('lrm-input-date')).componentInstance;
      const inputTimeComponent = testFixture.debugElement.query(By.css('lrm-input-time')).componentInstance;

      // test
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(inputDateComponent.disabled).toBeTrue();
        expect(inputTimeComponent.disabled).toBeTrue();
      });
    });
  }));
});
