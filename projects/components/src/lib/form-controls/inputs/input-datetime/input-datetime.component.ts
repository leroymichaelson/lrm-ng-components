import { Component, forwardRef, OnInit, AfterViewInit, Injector, ElementRef, Input, ViewChild } from '@angular/core';
import {NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';
import { InputBase } from '../../input-base';
import { InputDateComponent } from '../input-date/input-date.component';
import { InputTimeComponent } from '../input-time/input-time.component';

@Component({
  selector: 'lrm-input-datetime',
  templateUrl: './input-datetime.component.html',
  styleUrls: ['./input-datetime.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDateTimeComponent),
      multi: true
    }
  ]
})
export class InputDateTimeComponent extends InputBase<Date> implements OnInit, AfterViewInit {
  @ViewChild('defaultLabel', { static: true })
  defaultLabel: ElementRef;

  @ViewChild(InputDateComponent, null)
  dateInput: InputDateComponent;

  @ViewChild(InputTimeComponent, null)
  timeInput: InputTimeComponent;

  @Input()
  dateLabel: string;

  @Input()
  timeLabel: string;

  @Input()
  dateHint = '';

  @Input()
  timeHint = '';

  @Input()
  defaultTime: string;

  constructor(
    injector: Injector,
    element: ElementRef) {
    super(injector, element);
  }

  set dateValue(value: Date) {
    if (value !== this.value && value) {
      this.value = this.updateValueWithDate(value, this.value);
    }
  }

  set timeValue(value: Date) {
    if (value !== this.value && value) {
      this.value = this.updateValueWithTime(value, this.value);
    }
  }

  ngOnInit() {
    super.ngOnInit();

    if (this.value) {
      this.dateInput.value = this.value;
      this.timeInput.value = this.value;
    }
  }

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }

  protected valueChanged() {
    this.dateInput.value = this.value;
    this.timeInput.value = this.value;
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }

  private updateValueWithDate(dateValue: Date, currentValue: Date) {
    if (!dateValue) {
      return null;
    }

    if (!currentValue) {
      currentValue = new Date();
    }

    return new Date(
      dateValue.getFullYear(),
      dateValue.getMonth(),
      dateValue.getDate(),
      currentValue.getHours(),
      currentValue.getMinutes(),
      currentValue.getSeconds()
    );
  }

  private updateValueWithTime(timeValue: Date, currentValue: Date) {
    if (!timeValue) {
      return null;
    }

    if (!currentValue) {
      currentValue = new Date();
    }

    return new Date(
      currentValue.getFullYear(),
      currentValue.getMonth(),
      currentValue.getDate(),
      timeValue.getHours(),
      timeValue.getMinutes(),
      timeValue.getSeconds()
    );
  }
}
