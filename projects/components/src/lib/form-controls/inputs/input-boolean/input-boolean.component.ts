import { AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit } from '@angular/core';
import { InputBase } from '../../input-base';
import {NG_VALUE_ACCESSOR, NgControl, ValidatorFn} from '@angular/forms';

@Component({
  selector: 'lrm-input-boolean',
  templateUrl: './input-boolean.component.html',
  styleUrls: ['./input-boolean.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputBooleanComponent),
      multi: true
    }
  ]
})
export class InputBooleanComponent extends InputBase<boolean> implements AfterViewInit {

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }
}
