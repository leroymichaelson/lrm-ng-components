import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InputBooleanComponent } from './input-boolean.component';
import { MatCheckboxModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

describe('InputBooleanComponent', () => {
  let component: InputBooleanComponent;
  let fixture: ComponentFixture<InputBooleanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputBooleanComponent ],
      imports: [
        MatCheckboxModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputBooleanComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
