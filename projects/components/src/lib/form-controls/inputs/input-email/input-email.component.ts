import {AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit, ViewChild} from '@angular/core';
import {InputTextComponent} from '../input-text/input-text.component';
import {NG_VALUE_ACCESSOR, NgControl, ValidatorFn, Validators} from '@angular/forms';

@Component({
  selector: 'lrm-input-email',
  templateUrl: './input-email.component.html',
  styleUrls: ['./input-email.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputEmailComponent),
      multi: true
    }
  ]
})
export class InputEmailComponent extends InputTextComponent implements OnInit, AfterViewInit {

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  @ViewChild('defaultLabel', { static: true }) defaultLabel: ElementRef;
  @ViewChild('defaultPlaceholder', { static: true }) defaultPlaceholder: ElementRef;

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }

  ngOnInit() {
    if ( !this.label ) {
      this.label = this.defaultLabel.nativeElement.innerText;
    }
    if ( !this.placeholder ) {
      this.placeholder = this.defaultPlaceholder.nativeElement.innerText;
    }
  }

  protected validators(): ValidatorFn[] {
    return [
      Validators.email,
    ];
  }

  protected errorsOfInterest(): string[] {
    return ['email'].concat(super.errorsOfInterest());
  }
}
