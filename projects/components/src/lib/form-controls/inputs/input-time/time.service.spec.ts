import { TestBed } from '@angular/core/testing';
import {TimeService} from './time.service';

describe('TimeService', () => {
  let service: TimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimeService]
    });

    service = TestBed.get(TimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('formatTime()', () => {
    expect( service.formatTime('1')).toBe( '0100', '0100' );
    expect( service.formatTime('12')).toBe( '1200', '12' );
    expect( service.formatTime('123')).toBe( '1230', '123' );
    expect( service.formatTime('1234')).toBe( '1234', '1234' );

    expect( service.formatTime('1:0')).toBe( '0100', '1:0' );
    expect( service.formatTime('1:04')).toBe( '0104', '1:04' );

    expect( service.formatTime('abc')).toBe( '', 'abc' );

//    expect( service.formatTime('12045')).toBe( '', '12045' );
    expect( service.formatTime('12:045')).toBe( '', '12:045' );
  });

  it('isValidTime()', () => {
    expect( service.isValidTime('1234')).toBe(true, '1234');
    expect( service.isValidTime(null)).toBe(false, 'null');
    expect( service.isValidTime('')).toBe(false, 'empty string');
    expect( service.isValidTime('1')).toBe(false, '1');
    expect( service.isValidTime('12')).toBe(false, '12');
    expect( service.isValidTime('123')).toBe(false, '123');
    expect( service.isValidTime('0160')).toBe(false, '0160');
    expect( service.isValidTime('2500')).toBe(false, '2500');
    expect( service.isValidTime('2560')).toBe(false, '2560');

    expect( service.isValidTime('2359')).toBe(true, '2359');
  });

  it('convertTimeStringToDate()', () => {
    expect( service.convertTimeStringToDate('1234')).not.toBe(null, '1234');
    expect( service.convertTimeStringToDate(null)).toBe(null, 'null');
    expect( service.convertTimeStringToDate('')).toBe(null, 'empty string');
    expect( service.convertTimeStringToDate('1')).toBe(null, '1');
    expect( service.convertTimeStringToDate('12')).toBe(null, '12');
    expect( service.convertTimeStringToDate('123')).toBe(null, '123');
    expect( service.convertTimeStringToDate('0160')).toBe(null, '0160');
    expect( service.convertTimeStringToDate('2500')).toBe(null, '2500');
    expect( service.convertTimeStringToDate('2560')).toBe(null, '2560');

    expect( service.convertTimeStringToDate('2359')).not.toBe(null, '2359');
  });


});
