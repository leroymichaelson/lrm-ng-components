import { AfterViewInit, Component, ElementRef, forwardRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import {NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';
import { InputBase } from '../../input-base';
import { TimeService } from '../../../services/time.service';

@Component({
  selector: 'lrm-input-time',
  templateUrl: './input-time.component.html',
  styleUrls: ['./input-time.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTimeComponent),
      multi: true,
    },
  ],
})
export class InputTimeComponent extends InputBase<Date>
  implements OnInit, AfterViewInit {
  @ViewChild('defaultLabel', { static: true })
  defaultLabel: ElementRef;

  @Input() set defaultTime(timeValue: string) {
    if (timeValue) {
      this.defaultTimeString = timeValue;
    }
  }

  private defaultTimeString = '00:00';

  // tslint:disable-next-line:variable-name
  private _displayValue: string;
  set displayValue(time: string) {
    this._displayValue = time;
  }
  get displayValue(): string {
    return this._displayValue;
  }

  constructor(
    injector: Injector,
    element: ElementRef,
    private timeService: TimeService
  ) {
    super(injector, element);
  }

  ngOnInit() {
    super.ngOnInit();

    if (!this.label) {
      this.label = this.defaultLabel.nativeElement.innerText;
    }
  }

  ngAfterViewInit(): void {
    this.setNgControl(this.injector.get(NgControl));
    super.ngAfterViewInit();
  }

  public onFocus() {
    if (!this.value) {
      this.displayValue = this.defaultTimeString;
      this.convertDisplayValueToValue();
    }
  }

  public onBlur() {
    this.correctDisplayValue();
    this.convertDisplayValueToValue();

    if (
      this.displayValue === this.defaultTimeString ||
      this.displayValue === ''
    ) {
      this.displayValue = '';
      this.value = null;
    }
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }

  protected valueChanged() {
    this.setDisplayValueFromValue();
  }

  private setDisplayValueFromValue() {
    if (this.value) {
      this._displayValue = this.timeService.convertDateToTimeString(this.value);
    } else {
      this._displayValue = '';
    }
  }

  private correctDisplayValue() {
    if (this.displayValue !== '') {
      const formattedTime = this.timeService.formatTime(this.displayValue);

      if (formattedTime.match(/^([0-1]?[0-9]|2[0-3])[0-5][0-9]$/)) {
        this.displayValue = `${formattedTime.slice(0, 2)}:${formattedTime.slice(
          2,
          4
        )}`;
      }
    }
  }

  private convertDisplayValueToValue() {
    const convertedDate: Date = this.timeService.convertTimeStringToDate(
      this.displayValue.replace(':', '')
    );

    if (convertedDate) {
      this.value = convertedDate;
    }
  }
}
