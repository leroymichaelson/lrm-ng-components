import { async, TestBed } from '@angular/core/testing';
import { InputTimeComponent } from './input-time.component';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  template: `<form [formGroup]="form">
      <lrm-input-time formControlName="time" defaultTime="22:22"></lrm-input-time>
    </form>`
})
export class SimpleTestComponent {
  form = new FormGroup({
    time: new FormControl('')
  });
}

describe('InputTimeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InputTimeComponent,
        SimpleTestComponent
      ],
      imports: [
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  it('should create input time component', () => {
    // given / test
    const testFixture = TestBed.createComponent(InputTimeComponent);
    const inputTimeComponent = testFixture.componentInstance;

    // expect
    expect(inputTimeComponent).toBeTruthy();
  });

  it('should create simple test component', () => {
    // given / test
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    const testComponent = testFixture.componentInstance;

    // expect
    expect(testComponent).toBeTruthy();
  });

  it('should display the default time, set on the component, when clicking it', async(() => {
    // given
    const testFixture = TestBed.createComponent(SimpleTestComponent);
    testFixture.detectChanges();

    testFixture.whenStable().then(() => {
      testFixture.detectChanges();

      const timeValue = testFixture.debugElement
        .query(By.css('lrm-input-time[ng-reflect-name="time"] input'))
        .nativeElement;

      // test
      timeValue.click();
      testFixture.detectChanges();

      testFixture.whenStable().then(() => {
        testFixture.detectChanges();

        // expect
        expect(timeValue.value).toBe('22:22');
      });
    });
  }));
});
