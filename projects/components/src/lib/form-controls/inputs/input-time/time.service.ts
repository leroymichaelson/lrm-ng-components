import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  /**
   * Takes in a string with up to four digits with an optional colon and
   * attempts to convert it to the format 9999.
   *
   * It does not ensure that the digits are appropriate for a time value.
   */
  formatTime(time: string ): string {
    let xyz: string = time;

    xyz = xyz.toLocaleLowerCase();
    // Doing the work for am and pm
    if (xyz.includes('a') || xyz.includes('p')) {
      if (xyz.includes('a')) {
        if (xyz.length === 2 && xyz.substr(1, 2) === 'a') {
          xyz = '0' + xyz.substr(0, 1) + '00';
        } else if (xyz.length === 3) {
          if (xyz.substr(1, 3) === 'am') {
            xyz = '0' + xyz.substr(0, 1) + '00';
          } else if (xyz.substr(2, 3) === 'a') {
            if (+xyz.substr(0, 2) >= 12) {
              xyz = '';
            } else {
              xyz = xyz.substr(0, 2) + '00';
            }
          } else { xyz = ''; }
        } else if (xyz.length === 4 && xyz.substr(2, 4) === 'am') {
          if (+xyz.substr(0, 2) >= 12) {
            xyz = '';
          } else {
            xyz = xyz.substr(0, 2) + '00';
          }
        } else { xyz = ''; }
      } else {
        if (xyz.length === 2 && xyz.substr(1, 2) === 'p') {
          xyz = (12 + +xyz.substr(0, 1)).toString() + '00';
        } else if (xyz.length === 3) {
          if (xyz.substr(1, 3) === 'pm') {
            xyz = (12 + +xyz.substr(0, 1)).toString() + '00';
          } else if (xyz.substr(2, 3) === 'p') {
            xyz = (12 + +xyz.substr(0, 2)).toString() + '00';
          } else { xyz = ''; }
        } else if (xyz.length === 4 && xyz.substr(2, 4) === 'pm') {
          if (xyz.substr(0, 2) === '12') {
            xyz = xyz.substr(0, 2) + '00';
          } else {
            xyz = (12 + +xyz.substr(0, 2)).toString() + '00';
          }
        } else { xyz = ''; }
      }
      // Doing the work for time with a :
    } else if (xyz.includes(':')) {
      if (xyz.substr(0, 1).includes(':')) {
        if (xyz.length === 1) { xyz = '00:00'; } else
        if (xyz.length === 2) { xyz = '00:' + xyz.substr(1, 2) + '0'; } else
        if (xyz.length === 3) { xyz = '00:' + xyz.substr(1, 3); } else { xyz = ''; }
      } else if (xyz.substr(1, 1).includes(':')) {
        if (xyz.length === 2) { xyz = '0' + xyz.substr(0, 1) + ':00'; } else
        if (xyz.length === 3) { xyz = '0' + xyz.substr(0, 1) + ':' + '0' + xyz.substr(2, 3); } else
        if (xyz.length === 4) { xyz = '0' + xyz.substr(0, 1) + ':' + xyz.substr(2, 4); } else {
          xyz = '';
        }
      } else if (xyz.substr(2, 2).includes(':')) {
        if (xyz.length === 3) { xyz = xyz.substr(0, 2) + ':00'; } else
        if (xyz.length === 4) { xyz = xyz.substr(0, 3) + '0' + xyz.substr(3, 4); } else
        if (xyz.length !== 5) { xyz = ''; }
      } else { xyz = ''; }
      // Doing the work for just numbers
    } else {
      if (xyz.length === 1) { xyz = '0' + xyz + '00'; } else
      if (xyz.length === 2) { xyz = xyz + '00'; } else
      if (xyz.length === 3) {
        if (+xyz.substr(0, 1) > 2) { xyz = '0' + xyz; } else {
          xyz = xyz + '0';
        }
      }
    }

    return xyz.replace(':', '');
  }

  convertTimeStringToDate( time: string ): Date {
    if ( this.isValidTime(time) ) {
      const hours: number = +time.substr(0, 2);
      const minutes: number = +time.substr(2, 4);
      const date: Date = new Date();
      date.setHours(hours, minutes, 0, 0);
      return date;
    } else {
      return null;
    }
  }

  /**
   * Takes in a 4 digit string ("0934" which means 9:34am) and returns true if it represents
   * a valid time.
   */
  isValidTime( xyz: string ): boolean {
    return xyz != null
      && xyz !== ''
      && xyz.length === 4
      && Number(xyz.substring(0, 2)) < 24
      && Number(xyz.substring(2, 4)) < 60
      && Number(xyz) <= 2359;
  }

  convertDateToTimeString( date: Date ): string {
    if ( !date ) { return ''; }
    const hours: number = date.getHours();
    const minutes: number = date.getMinutes();
    return this.padToTwoDigits('' + hours) + ':' + this.padToTwoDigits('' + minutes);
  }

  private padToTwoDigits( value: string ): string {
    if ( value.length === 1 ) {
      return '0' + value;
    } else {
      return value;
    }
  }
}
