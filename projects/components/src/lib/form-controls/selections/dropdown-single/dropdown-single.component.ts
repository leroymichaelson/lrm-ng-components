import {
  AfterViewInit,
  Component,
  ElementRef,
  forwardRef,
  Injector, Input} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectionBase } from '../selection-base';

@Component({
  selector: 'lrm-dropdown-single',
  templateUrl: './dropdown-single.component.html',
  styleUrls: ['./dropdown-single.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownSingleComponent),
      multi: true
    }
  ]
})
export class DropdownSingleComponent extends SelectionBase<string> implements AfterViewInit {

  @Input() placeholder;

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }
}
