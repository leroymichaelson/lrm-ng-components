import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DropdownMultiComponent } from './dropdown-multi.component';
import { MatFormFieldModule, MatSelectModule, MatOptionModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

describe('DropdownMultiComponent', () => {
  let component: DropdownMultiComponent;
  let fixture: ComponentFixture<DropdownMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownMultiComponent ],
      imports: [
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMultiComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
