import { AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ValidatorFn } from '@angular/forms';
import { SelectionBase } from '../selection-base';

@Component({
  selector: 'lrm-dropdown-multi',
  templateUrl: './dropdown-multi.component.html',
  styleUrls: ['./dropdown-multi.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownMultiComponent),
      multi: true
    }
  ]
})
export class DropdownMultiComponent extends SelectionBase<string[]> implements AfterViewInit {

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }

  protected valueChanged() {
    super.valueChanged();
  }
}
