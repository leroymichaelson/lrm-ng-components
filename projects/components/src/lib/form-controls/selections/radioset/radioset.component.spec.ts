import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RadiosetComponent } from './radioset.component';
import { MatRadioModule, MatFormFieldModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

describe('RadiosetComponent', () => {
  let component: RadiosetComponent;
  let fixture: ComponentFixture<RadiosetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadiosetComponent ],
      imports: [
        MatRadioModule,
        MatFormFieldModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiosetComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
