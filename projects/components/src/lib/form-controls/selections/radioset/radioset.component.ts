import {AfterViewInit, Component, ElementRef, forwardRef, Injector} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {SelectionBase} from '../selection-base';

@Component({
  selector: 'lrm-radioset',
  templateUrl: './radioset.component.html',
  styleUrls: ['./radioset.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadiosetComponent),
      multi: true
    }
  ]
})
export class RadiosetComponent extends SelectionBase<string> implements AfterViewInit {

  static counter = 0;

  groupName: string;

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
    this.groupName = 'Radioset-' + RadiosetComponent.counter++;
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }
}
