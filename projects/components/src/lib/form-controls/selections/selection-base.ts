import {InputBase} from '../input-base';
import {AfterViewInit, ContentChildren, ElementRef, Injector, OnInit, QueryList} from '@angular/core';
import {OptionComponent} from '../option/option.component';

export abstract class SelectionBase<T> extends InputBase<T> implements OnInit, AfterViewInit {

  @ContentChildren(OptionComponent) options: QueryList<OptionComponent>;

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
//    console.log( 'SELECTIONBASE: ' + this.options.length );
    this.options.forEach((option) => {
//      console.log( 'SELECTIONBASE .. : ' + option.name + ' = ' + option.value );
//      this.choices.push( option.name );
    });
  }

  hasAnyError() {
    if (this.formControl) {
      return !this.formControl.valid;
    } else {
      return false;
    }
  }
}
