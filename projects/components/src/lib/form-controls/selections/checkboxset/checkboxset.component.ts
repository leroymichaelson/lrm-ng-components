import {AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit, ViewChild} from '@angular/core';
import { NG_VALUE_ACCESSOR, ValidatorFn } from '@angular/forms';
import { SelectionBase } from '../selection-base';

@Component({
  selector: 'lrm-checkboxset',
  templateUrl: './checkboxset.component.html',
  styleUrls: ['./checkboxset.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxsetComponent),
      multi: true
    }
  ]
})
export class CheckboxsetComponent extends SelectionBase<string[]> implements OnInit, AfterViewInit {

  static counter = 0;

  groupName: string;

  constructor(injector: Injector, element: ElementRef) {
    super(injector, element);
    this.groupName = 'Checkboxset-' + CheckboxsetComponent.counter++;
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  protected errorsOfInterest(): string[] {
    return ['required'];
  }
}
