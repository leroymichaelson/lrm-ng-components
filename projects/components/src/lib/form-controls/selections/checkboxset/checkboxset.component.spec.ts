import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CheckboxsetComponent } from './checkboxset.component';
import { MatSelectModule, MatListModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

describe('CheckboxsetComponent', () => {
  let component: CheckboxsetComponent;
  let fixture: ComponentFixture<CheckboxsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxsetComponent ],
      imports: [
        MatSelectModule,
        MatListModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxsetComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
