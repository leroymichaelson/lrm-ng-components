import {NgModule} from '@angular/core';
import { PageComponent } from './layout/page/page.component';
import { CardComponent } from './layout/card/card.component';
import { CommonModule } from '@angular/common';
import { ErrorIndicatorComponent } from './error-indicator/error-indicator.component';
import { LoadingContainerComponent } from './loading-container/loading-container.component';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';
import {
  ActionAddComponent,
  ActionBackComponent,
  ActionCancelComponent,
  ActionCloseComponent,
  ActionCompleteComponent,
  ActionContinueComponent,
  ActionCreateComponent,
  ActionDeclineComponent,
  ActionDeleteComponent,
  ActionDenyComponent,
  ActionDownloadComponent,
  ActionEditComponent,
  ActionGoComponent,
  ActionLogoutComponent,
  ActionNextComponent,
  ActionNoComponent,
  ActionPreviousComponent,
  ActionPrintComponent,
  ActionRefreshComponent,
  ActionRemoveComponent,
  ActionResetComponent,
  ActionSaveComponent,
  ActionSearchComponent,
  ActionSelectComponent,
  ActionSubmitComponent,
  ActionUploadComponent,
  ActionWaitComponent,
  ActionYesComponent,
} from './actions/actions.component';
import { CheckboxsetComponent } from './form-controls/selections/checkboxset/checkboxset.component';
import { DropdownMultiComponent } from './form-controls/selections/dropdown-multi/dropdown-multi.component';
import { DropdownSingleComponent } from './form-controls/selections/dropdown-single/dropdown-single.component';
import { OptionComponent } from './form-controls/option/option.component';
import { RadiosetComponent } from './form-controls/selections/radioset/radioset.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { InputBooleanComponent } from './form-controls/inputs/input-boolean/input-boolean.component';
import { InputTextComponent } from './form-controls/inputs/input-text/input-text.component';
import { InputEmailComponent } from './form-controls/inputs/input-email/input-email.component';
import { InputPasswordComponent } from './form-controls/inputs/input-password/input-password.component';
import { SwitchComponent } from './form-controls/inputs/switch/switch.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputDateComponent } from './form-controls/inputs/input-date/input-date.component';
import { InputPhoneComponent } from './form-controls/inputs/input-phone/input-phone.component';
import { InputSSNComponent } from './form-controls/inputs/input-ssn/input-ssn.component';
import { ActionExportComponent } from './actions/action-export.component';
import { ActionContainerComponent } from './action-container/action-container.component';
import { DialogActionsComponent } from './dialog-actions/dialog-actions.component';
import { DialogBodyComponent } from './dialog-body/dialog-body.component';
import { DialogComponent } from './dialog/dialog.component';
import { InputTimeComponent } from './form-controls/inputs/input-time/input-time.component';
import { PageToolsComponent } from './layout/page-tools/page-tools.component';
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatIconModule,
} from '@angular/material';
import { CardActionsComponent } from './layout/card/card-actions/card-actions.component';
import { PopupComponent } from './popup/popup.component';
import { PopoverComponent } from './popover/popover-component/popover.component';
import { LabelComponent } from './label/label.component';
import { LabelValueListComponent } from './label-value-list/label-value-list.component';
import { PopoverTitleComponent } from './popover/popover-title/popover-title.component';
import { TextMaskModule } from 'angular2-text-mask';
import { OutputPhoneComponent } from './outputs/output-phone/output-phone.component';
import { PopoverService } from './popover/popover.service';
import { InputDateTimeComponent } from './form-controls/inputs/input-datetime/input-datetime.component';
import { PopoverContentComponent } from './popover/popover-content/popover-content.component';
import { TabbedCardComponent } from './layout/tabbed-card/tabbed-card.component';
import { TabComponent } from './layout/tab/tab.component';
import { InputTextAreaComponent } from './form-controls/inputs/input-textarea/input-textarea.component';

const actions = [
  ActionAddComponent,
  ActionBackComponent,
  ActionCancelComponent,
  ActionCloseComponent,
  ActionCompleteComponent,
  ActionContinueComponent,
  ActionCreateComponent,
  ActionDeleteComponent,
  ActionDenyComponent,
  ActionDeclineComponent,
  ActionDownloadComponent,
  ActionExportComponent,
  ActionEditComponent,
  ActionGoComponent,
  ActionLogoutComponent,
  ActionNextComponent,
  ActionNoComponent,
  ActionPreviousComponent,
  ActionPrintComponent,
  ActionRefreshComponent,
  ActionRemoveComponent,
  ActionResetComponent,
  ActionSaveComponent,
  ActionSearchComponent,
  ActionSelectComponent,
  ActionSubmitComponent,
  ActionUploadComponent,
  ActionWaitComponent,
  ActionYesComponent
];

const components = [
  actions,
  ActionContainerComponent,
  CheckboxsetComponent,
  ConfirmationDialogComponent,
  DialogComponent,
  DialogBodyComponent,
  DialogActionsComponent,
  DropdownMultiComponent,
  DropdownSingleComponent,
  ErrorIndicatorComponent,
  InputBooleanComponent,
  InputDateComponent,
  InputTextComponent,
  InputTextAreaComponent,
  InputTimeComponent,
  InputDateTimeComponent,
  InputEmailComponent,
  InputPasswordComponent,
  InputPhoneComponent,
  InputSSNComponent,
  LoadingIndicatorComponent,
  LoadingContainerComponent,
  PageComponent,
  PageToolsComponent,
  PopupComponent,
  PopoverComponent,
  PageToolsComponent,
  CardComponent,
  CardActionsComponent,
  OptionComponent,
  RadiosetComponent,
  SwitchComponent,
  LabelComponent,
  LabelValueListComponent,
  PopoverTitleComponent,
  OutputPhoneComponent,
  PopoverContentComponent,
  TabbedCardComponent,
  TabComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatIconModule,
    TextMaskModule,
  ],
  entryComponents: [PopoverComponent],
  exports: [components],
  providers: []
})
export class LRMComponentsModule {}
