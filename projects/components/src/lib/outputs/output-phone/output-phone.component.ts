import { Component, Input, OnInit } from '@angular/core';
import { conformToMask } from 'angular2-text-mask';
import { PhoneService } from '../../services/phone.service';
import { ICountry } from '../../form-controls/inputs/input-phone/country';

@Component({
  selector: 'lrm-output-phone',
  template: '{{formattedPhoneNumber}}'
})
export class OutputPhoneComponent implements OnInit {
  private defaultCountry = 'US';

  @Input()
  public value = '';

  @Input()
  public country = this.defaultCountry;

  constructor(private phoneService: PhoneService) { }

  public formattedPhoneNumber = '';

  ngOnInit(): void {
    if (!this.value || this.value === '') {
      return;
    }

    this.value = this.value.replace(/[^\d\+]+/, '');

    if (this.value === '') {
      return;
    }

    let country = this.phoneService.getCountryByPhoneValue(this.value);

    if (!country) {
      country = this.phoneService.getCountry(this.country, this.defaultCountry);
      this.value = `+${country.dialCode} ${this.value}`;
    }

    this.formattedPhoneNumber = this.formatPhoneNumber(this.value, country);
  }

  private formatPhoneNumber(phoneNumber: string, country: ICountry): string {
    if (!country.phoneMask || country.phoneMask.length <= 0) {
      return `+${country.dialCode} ${phoneNumber.replace(/\D+/g, '').substring(country.dialCode.length)}`;
    } else {
      const result = conformToMask(this.value, country.phoneMask, { guide: false });

      return result.conformedValue;
    }
  }
}
