import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { OutputPhoneComponent } from './output-phone.component';
import { TextMaskModule } from 'angular2-text-mask';

describe('OutputPhoneComponent', () => {
  let component: OutputPhoneComponent;
  let fixture: ComponentFixture<OutputPhoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OutputPhoneComponent
      ],
      imports: [
        TextMaskModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputPhoneComponent);
    component = fixture.componentInstance;
  });

  it('should create output phone component', () => {
    // given / test / expect
    expect(component).toBeTruthy();
  });

  it('should format nothing if input is not set', () => {
    // given / test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('');
  });

  it('should format nothing if input is empty', () => {
    // given
    component.value = '';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('');
  });

  it('should format phone number if input is set to +1 7208879010', () => {
    // given
    component.value = '+1 7208879010';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+1 (720) 887-9010');
  });

  it('should format phone number if input is set to +17208889011', () => {
    // given
    component.value = '+17208889011';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+1 (720) 888-9011');
  });

  it('should format phone number with default country if phone number is 0000000000', () => {
    // given
    component.value = '0000000000';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+1 (000) 000-0000');
  });

  it('should format phone number with country `nl` if phone number is 0649785309', () => {
    // given
    component.value = '0649785309';
    component.country = 'nl';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+31 06 49785309');
  });

  it('should format phone number at best when given half a number: +16667', () => {
    // given
    component.value = '+16667';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+1 (666) 7');
  });

  it('should format nothing when no numeric value is given: `abc`', () => {
    // given
    component.value = 'abc';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('');
  });

  it('should format phone number at best when value has invalid characters: `+fgh456`', () => {
    // given
    component.value = '+fgh456';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+45 6');
  });

  it('should format phone number the basic way, when there is no input mask for it: `+13451213213213213`', () => {
    // given
    component.value = '+13451213213213213';

    // test
    fixture.detectChanges();

    // expect
    expect(fixture.nativeElement.innerText).toBe('+1345 1213213213213');
  });
});
