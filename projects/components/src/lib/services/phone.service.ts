import { Injectable } from '@angular/core';
import { ICountry } from '../form-controls/inputs/input-phone/country';

@Injectable({
  providedIn: 'root',
})
export class PhoneService {
  public getCountry(iso2Code: string, defaultCode: string): ICountry {
    iso2Code = iso2Code.toLowerCase();
    const countries = this.getAllCountries();
    const result = countries.filter((x) => x.iso2Code === iso2Code);

    if (result && result.length > 0) {
      return result[0];
    }

    return countries.filter((x) => x.iso2Code === defaultCode)[0];
  }

  public getCountryByPhoneValue(phoneValue: string): ICountry {
    phoneValue = phoneValue.replace(/\D+/g, '');
    let mostLikelyCountry: ICountry;

    for (let i = 1; i <= 4; i++) {
      const country = this.getCountryByDialCode(phoneValue.substring(0, i));

      if (country) {
        mostLikelyCountry = country;
      }
    }

    return mostLikelyCountry ? mostLikelyCountry : null;
  }

  public getCountryByDialCode(dialCode: string): ICountry {
    const result = this.getAllCountries().filter((x) => x.dialCode === dialCode);

    if (result && result.length > 0) {
      return result.sort((a, b) => a.order - b.order)[0];
    }

    return null;
  }

  public searchCountry(phrase: string) {
    phrase = phrase.toLowerCase();

    return this.getAllCountries().filter(
      (x) =>
        x.name.toLowerCase().startsWith(phrase) ||
        x.iso2Code.startsWith(phrase)
    );
  }

  public getAllCountries() {
    return [
      {
        name: 'Afghanistan',
        iso2Code: 'af',
        dialCode: '93',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Albania',
        iso2Code: 'al',
        dialCode: '355',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Algeria',
        iso2Code: 'dz',
        dialCode: '213',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'American Samoa',
        iso2Code: 'as',
        dialCode: '1684',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Andorra',
        iso2Code: 'ad',
        dialCode: '376',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Angola',
        iso2Code: 'ao',
        dialCode: '244',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Anguilla',
        iso2Code: 'ai',
        dialCode: '1264',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Antigua and Barbuda',
        iso2Code: 'ag',
        dialCode: '1268',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Argentina',
        iso2Code: 'ar',
        dialCode: '54',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Armenia',
        iso2Code: 'am',
        dialCode: '374',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Aruba',
        iso2Code: 'aw',
        dialCode: '297',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Australia',
        iso2Code: 'au',
        dialCode: '61',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Austria',
        iso2Code: 'at',
        dialCode: '43',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Azerbaijan',
        iso2Code: 'az',
        dialCode: '994',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bahamas',
        iso2Code: 'bs',
        dialCode: '1242',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bahrain',
        iso2Code: 'bh',
        dialCode: '973',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bangladesh',
        iso2Code: 'bd',
        dialCode: '880',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Barbados',
        iso2Code: 'bb',
        dialCode: '1246',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Belarus',
        iso2Code: 'by',
        dialCode: '375',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Belgium',
        iso2Code: 'be',
        dialCode: '32',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Belize',
        iso2Code: 'bz',
        dialCode: '501',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Benin',
        iso2Code: 'bj',
        dialCode: '229',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bermuda',
        iso2Code: 'bm',
        dialCode: '1441',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bhutan',
        iso2Code: 'bt',
        dialCode: '975',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bolivia',
        iso2Code: 'bo',
        dialCode: '591',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bosnia and Herzegovina',
        iso2Code: 'ba',
        dialCode: '387',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Botswana',
        iso2Code: 'bw',
        dialCode: '267',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Brazil',
        iso2Code: 'br',
        dialCode: '55',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'British Indian Ocean Territory',
        iso2Code: 'io',
        dialCode: '246',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'British Virgin Islands',
        iso2Code: 'vg',
        dialCode: '1284',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Brunei',
        iso2Code: 'bn',
        dialCode: '673',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Bulgaria',
        iso2Code: 'bg',
        dialCode: '359',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Burkina Faso',
        iso2Code: 'bf',
        dialCode: '226',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Burundi',
        iso2Code: 'bi',
        dialCode: '257',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Cambodia',
        iso2Code: 'kh',
        dialCode: '855',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Cameroon',
        iso2Code: 'cm',
        dialCode: '237',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Canada',
        iso2Code: 'ca',
        dialCode: '1',
        phoneMask: [
          '+',
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 1
      },
      {
        name: 'Cape Verde',
        iso2Code: 'cv',
        dialCode: '238',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Caribbean Netherlands',
        iso2Code: 'bq',
        dialCode: '599',
        phoneMask: [],
        phonePattern: '',
        order: 1
      },
      {
        name: 'Cayman Islands',
        iso2Code: 'ky',
        dialCode: '1345',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Central African Republic',
        iso2Code: 'cf',
        dialCode: '236',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Chad',
        iso2Code: 'td',
        dialCode: '235',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Chile',
        iso2Code: 'cl',
        dialCode: '56',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'China',
        iso2Code: 'cn',
        dialCode: '86',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Colombia',
        iso2Code: 'co',
        dialCode: '57',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Comoros',
        iso2Code: 'km',
        dialCode: '269',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Congo',
        iso2Code: 'cd',
        dialCode: '243',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Congo',
        iso2Code: 'cg',
        dialCode: '242',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Cook Islands',
        iso2Code: 'ck',
        dialCode: '682',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Costa Rica',
        iso2Code: 'cr',
        dialCode: '506',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Côte d’Ivoire',
        iso2Code: 'ci',
        dialCode: '225',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Croatia',
        iso2Code: 'hr',
        dialCode: '385',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Cuba',
        iso2Code: 'cu',
        dialCode: '53',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Curaçao',
        iso2Code: 'cw',
        dialCode: '599',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Cyprus',
        iso2Code: 'cy',
        dialCode: '357',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Czech Republic',
        iso2Code: 'cz',
        dialCode: '420',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Denmark',
        iso2Code: 'dk',
        dialCode: '45',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Djibouti',
        iso2Code: 'dj',
        dialCode: '253',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Dominica',
        iso2Code: 'dm',
        dialCode: '1767',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Dominican Republic',
        iso2Code: 'do',
        dialCode: '1',
        phoneMask: [],
        phonePattern: '',
        order: 2
      },
      {
        name: 'Ecuador',
        iso2Code: 'ec',
        dialCode: '593',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Egypt',
        iso2Code: 'eg',
        dialCode: '20',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'El Salvador',
        iso2Code: 'sv',
        dialCode: '503',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Equatorial Guinea',
        iso2Code: 'gq',
        dialCode: '240',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Eritrea',
        iso2Code: 'er',
        dialCode: '291',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Estonia',
        iso2Code: 'ee',
        dialCode: '372',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Ethiopia',
        iso2Code: 'et',
        dialCode: '251',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Falkland Islands',
        iso2Code: 'fk',
        dialCode: '500',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Faroe Islands',
        iso2Code: 'fo',
        dialCode: '298',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Fiji',
        iso2Code: 'fj',
        dialCode: '679',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Finland',
        iso2Code: 'fi',
        dialCode: '358',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'France',
        iso2Code: 'fr',
        dialCode: '33',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'French Guiana',
        iso2Code: 'gf',
        dialCode: '594',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'French Polynesia',
        iso2Code: 'pf',
        dialCode: '689',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Gabon',
        iso2Code: 'ga',
        dialCode: '241',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Gambia',
        iso2Code: 'gm',
        dialCode: '220',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Georgia',
        iso2Code: 'ge',
        dialCode: '995',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Germany',
        iso2Code: 'de',
        dialCode: '49',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Ghana',
        iso2Code: 'gh',
        dialCode: '233',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Gibraltar',
        iso2Code: 'gi',
        dialCode: '350',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Greece',
        iso2Code: 'gr',
        dialCode: '30',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Greenland',
        iso2Code: 'gl',
        dialCode: '299',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Grenada',
        iso2Code: 'gd',
        dialCode: '1473',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Guadeloupe',
        iso2Code: 'gp',
        dialCode: '590',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Guam',
        iso2Code: 'gu',
        dialCode: '1671',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Guatemala',
        iso2Code: 'gt',
        dialCode: '502',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Guinea',
        iso2Code: 'gn',
        dialCode: '224',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Guinea-Bissau',
        iso2Code: 'gw',
        dialCode: '245',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Guyana',
        iso2Code: 'gy',
        dialCode: '592',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Haiti',
        iso2Code: 'ht',
        dialCode: '509',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Honduras',
        iso2Code: 'hn',
        dialCode: '504',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Hong Kong',
        iso2Code: 'hk',
        dialCode: '852',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Hungary',
        iso2Code: 'hu',
        dialCode: '36',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Iceland',
        iso2Code: 'is',
        dialCode: '354',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'India',
        iso2Code: 'in',
        dialCode: '91',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Indonesia',
        iso2Code: 'id',
        dialCode: '62',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Iran',
        iso2Code: 'ir',
        dialCode: '98',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Iraq',
        iso2Code: 'iq',
        dialCode: '964',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Ireland',
        iso2Code: 'ie',
        dialCode: '353',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Israel',
        iso2Code: 'il',
        dialCode: '972',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Italy',
        iso2Code: 'it',
        dialCode: '39',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Jamaica',
        iso2Code: 'jm',
        dialCode: '1876',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Japan',
        iso2Code: 'jp',
        dialCode: '81',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Jordan',
        iso2Code: 'jo',
        dialCode: '962',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Kazakhstan',
        iso2Code: 'kz',
        dialCode: '7',
        phoneMask: [
          '+',
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 1
      },
      {
        name: 'Kenya',
        iso2Code: 'ke',
        dialCode: '254',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Kiribati',
        iso2Code: 'ki',
        dialCode: '686',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Kuwait',
        iso2Code: 'kw',
        dialCode: '965',
        phoneMask: [],
        phonePattern: '',
        order: 0,
      },
      {
        name: 'Kyrgyzstan',
        iso2Code: 'kg',
        dialCode: '996',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Laos',
        iso2Code: 'la',
        dialCode: '856',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Latvia',
        iso2Code: 'lv',
        dialCode: '371',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Lebanon',
        iso2Code: 'lb',
        dialCode: '961',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Lesotho',
        iso2Code: 'ls',
        dialCode: '266',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Liberia',
        iso2Code: 'lr',
        dialCode: '231',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Libya',
        iso2Code: 'ly',
        dialCode: '218',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Liechtenstein',
        iso2Code: 'li',
        dialCode: '423',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Lithuania',
        iso2Code: 'lt',
        dialCode: '370',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Luxembourg',
        iso2Code: 'lu',
        dialCode: '352',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Macau',
        iso2Code: 'mo',
        dialCode: '853',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Macedonia',
        iso2Code: 'mk',
        dialCode: '389',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Madagascar',
        iso2Code: 'mg',
        dialCode: '261',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Malawi',
        iso2Code: 'mw',
        dialCode: '265',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Malaysia',
        iso2Code: 'my',
        dialCode: '60',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Maldives',
        iso2Code: 'mv',
        dialCode: '960',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Mali',
        iso2Code: 'ml',
        dialCode: '223',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Malta',
        iso2Code: 'mt',
        dialCode: '356',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Marshall Islands',
        iso2Code: 'mh',
        dialCode: '692',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Martinique',
        iso2Code: 'mq',
        dialCode: '596',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Mauritania',
        iso2Code: 'mr',
        dialCode: '222',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Mauritius',
        iso2Code: 'mu',
        dialCode: '230',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Mexico',
        iso2Code: 'mx',
        dialCode: '52',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Micronesia',
        iso2Code: 'fm',
        dialCode: '691',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Moldova',
        iso2Code: 'md',
        dialCode: '373',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Monaco',
        iso2Code: 'mc',
        dialCode: '377',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Mongolia',
        iso2Code: 'mn',
        dialCode: '976',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Montenegro',
        iso2Code: 'me',
        dialCode: '382',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Montserrat',
        iso2Code: 'ms',
        dialCode: '1664',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Morocco',
        iso2Code: 'ma',
        dialCode: '212',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Mozambique',
        iso2Code: 'mz',
        dialCode: '258',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Myanmar',
        iso2Code: 'mm',
        dialCode: '95',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Namibia',
        iso2Code: 'na',
        dialCode: '264',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Nauru',
        iso2Code: 'nr',
        dialCode: '674',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Nepal',
        iso2Code: 'np',
        dialCode: '977',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Netherlands',
        iso2Code: 'nl',
        dialCode: '31',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'New Caledonia',
        iso2Code: 'nc',
        dialCode: '687',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'New Zealand',
        iso2Code: 'nz',
        dialCode: '64',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Nicaragua',
        iso2Code: 'ni',
        dialCode: '505',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Niger',
        iso2Code: 'ne',
        dialCode: '227',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Nigeria',
        iso2Code: 'ng',
        dialCode: '234',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Niue',
        iso2Code: 'nu',
        dialCode: '683',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Norfolk Island',
        iso2Code: 'nf',
        dialCode: '672',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'North Korea',
        iso2Code: 'kp',
        dialCode: '850',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Northern Mariana Islands',
        iso2Code: 'mp',
        dialCode: '1670',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Norway',
        iso2Code: 'no',
        dialCode: '47',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Oman',
        iso2Code: 'om',
        dialCode: '968',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Pakistan',
        iso2Code: 'pk',
        dialCode: '92',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Palau',
        iso2Code: 'pw',
        dialCode: '680',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Palestine',
        iso2Code: 'ps',
        dialCode: '970',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Panama',
        iso2Code: 'pa',
        dialCode: '507',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Papua New Guinea',
        iso2Code: 'pg',
        dialCode: '675',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Paraguay',
        iso2Code: 'py',
        dialCode: '595',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Peru',
        iso2Code: 'pe',
        dialCode: '51',
        phoneMask: [],
        phonePattern: '',
        order: 0,
      },
      {
        name: 'Philippines',
        iso2Code: 'ph',
        dialCode: '63',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Poland',
        iso2Code: 'pl',
        dialCode: '48',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Portugal',
        iso2Code: 'pt',
        dialCode: '351',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Puerto Rico',
        iso2Code: 'pr',
        dialCode: '1',
        phoneMask: [],
        phonePattern: '',
        order: 3
      },
      {
        name: 'Qatar',
        iso2Code: 'qa',
        dialCode: '974',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Réunion',
        iso2Code: 're',
        dialCode: '262',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Romania',
        iso2Code: 'ro',
        dialCode: '40',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Russia',
        iso2Code: 'ru',
        dialCode: '7',
        phoneMask: [
          '+',
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Rwanda',
        iso2Code: 'rw',
        dialCode: '250',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Saint Barthélemy',
        iso2Code: 'bl',
        dialCode: '590',
        phoneMask: [],
        phonePattern: '',
        order: 1
      },
      {
        name: 'Saint Helena',
        iso2Code: 'sh',
        dialCode: '290',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Saint Kitts and Nevis',
        iso2Code: 'kn',
        dialCode: '1869',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Saint Lucia',
        iso2Code: 'lc',
        dialCode: '1758',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Saint Martin',
        iso2Code: 'mf',
        dialCode: '590',
        phoneMask: [],
        phonePattern: '',
        order: 2
      },
      {
        name: 'Saint Pierre and Miquelon',
        iso2Code: 'pm',
        dialCode: '508',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Saint Vincent and the Grenadines',
        iso2Code: 'vc',
        dialCode: '1784',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Samoa',
        iso2Code: 'ws',
        dialCode: '685',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'San Marino',
        iso2Code: 'sm',
        dialCode: '378',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'São Tomé and Príncipe',
        iso2Code: 'st',
        dialCode: '239',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Saudi Arabia',
        iso2Code: 'sa',
        dialCode: '966',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Senegal',
        iso2Code: 'sn',
        dialCode: '221',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Serbia',
        iso2Code: 'rs',
        dialCode: '381',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Seychelles',
        iso2Code: 'sc',
        dialCode: '248',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Sierra Leone',
        iso2Code: 'sl',
        dialCode: '232',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Singapore',
        iso2Code: 'sg',
        dialCode: '65',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Sint Maarten',
        iso2Code: 'sx',
        dialCode: '1721',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Slovakia',
        iso2Code: 'sk',
        dialCode: '421',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Slovenia',
        iso2Code: 'si',
        dialCode: '386',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Solomon Islands',
        iso2Code: 'sb',
        dialCode: '677',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Somalia',
        iso2Code: 'so',
        dialCode: '252',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'South Africa',
        iso2Code: 'za',
        dialCode: '27',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'South Korea',
        iso2Code: 'kr',
        dialCode: '82',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'South Sudan',
        iso2Code: 'ss',
        dialCode: '211',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Spain',
        iso2Code: 'es',
        dialCode: '34',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Sri Lanka',
        iso2Code: 'lk',
        dialCode: '94',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Sudan',
        iso2Code: 'sd',
        dialCode: '249',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Suriname',
        iso2Code: 'sr',
        dialCode: '597',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Swaziland',
        iso2Code: 'sz',
        dialCode: '268',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Sweden',
        iso2Code: 'se',
        dialCode: '46',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Switzerland',
        iso2Code: 'ch',
        dialCode: '41',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Syria',
        iso2Code: 'sy',
        dialCode: '963',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Taiwan',
        iso2Code: 'tw',
        dialCode: '886',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Tajikistan',
        iso2Code: 'tj',
        dialCode: '992',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Tanzania',
        iso2Code: 'tz',
        dialCode: '255',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Thailand',
        iso2Code: 'th',
        dialCode: '66',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Timor-Leste',
        iso2Code: 'tl',
        dialCode: '670',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Togo',
        iso2Code: 'tg',
        dialCode: '228',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Tokelau',
        iso2Code: 'tk',
        dialCode: '690',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Tonga',
        iso2Code: 'to',
        dialCode: '676',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Trinidad and Tobago',
        iso2Code: 'tt',
        dialCode: '1868',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Tunisia',
        iso2Code: 'tn',
        dialCode: '216',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Turkey',
        iso2Code: 'tr',
        dialCode: '90',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Turkmenistan',
        iso2Code: 'tm',
        dialCode: '993',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Turks and Caicos Islands',
        iso2Code: 'tc',
        dialCode: '1649',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Tuvalu',
        iso2Code: 'tv',
        dialCode: '688',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'U.S. Virgin Islands',
        iso2Code: 'vi',
        dialCode: '1340',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Uganda',
        iso2Code: 'ug',
        dialCode: '256',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Ukraine',
        iso2Code: 'ua',
        dialCode: '380',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'United Arab Emirates',
        iso2Code: 'ae',
        dialCode: '971',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'United Kingdom',
        iso2Code: 'gb',
        dialCode: '44',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'United States',
        iso2Code: 'us',
        dialCode: '1',
        phoneMask: [
          '+',
          /\d/,
          ' ',
          '(',
          /\d/,
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Uruguay',
        iso2Code: 'uy',
        dialCode: '598',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Uzbekistan',
        iso2Code: 'uz',
        dialCode: '998',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Vanuatu',
        iso2Code: 'vu',
        dialCode: '678',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Vatican City',
        iso2Code: 'va',
        dialCode: '39',
        phoneMask: [
          '+',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ],
        phonePattern: '',
        order: 1
      },
      {
        name: 'Venezuela',
        iso2Code: 've',
        dialCode: '58',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Vietnam',
        iso2Code: 'vn',
        dialCode: '84',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Wallis and Futuna',
        iso2Code: 'wf',
        dialCode: '681',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Yemen',
        iso2Code: 'ye',
        dialCode: '967',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Zambia',
        iso2Code: 'zm',
        dialCode: '260',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
      {
        name: 'Zimbabwe',
        iso2Code: 'zw',
        dialCode: '263',
        phoneMask: [],
        phonePattern: '',
        order: 0
      },
    ];
  }
}
