import { TestBed } from '@angular/core/testing';
import { PhoneService } from './phone.service';

describe('PhoneService', () => {
  let service: PhoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhoneService],
    });

    service = TestBed.get(PhoneService);
  });

  it('should be created', () => {
    // given / test / expect
    expect(service).toBeTruthy();
  });

  it('should return the right country when calling getCountry(`nl`, `us`)', () => {
    // given
    const country = 'nl';

    // test
    const result = service.getCountry(country, 'us');

    // expect
    expect(result.name).toBe('Netherlands');
  });

  it('should return the default country when calling getCountry(`os`, `be`)', () => {
    // given
    const country = 'os'; // non existent

    // test
    const result = service.getCountry(country, 'be');

    // expect
    expect(result.name).toBe('Belgium');
  });

  it('should return null when calling getCountryByPhoneValue(``)', () => {
    // given
    const phoneValue = '';

    // test
    const result = service.getCountryByPhoneValue(phoneValue);

    // expect
    expect(result).toBeNull();
  });

  it('should return country France when calling getCountryByPhoneValue(`+33 12345678`)', () => {
    // given
    const phoneValue = '+33 12345678';

    // test
    const result = service.getCountryByPhoneValue(phoneValue);

    // expect
    expect(result.name).toBe('France');
  });

  it('should return country American Samoa when calling getCountryByPhoneValue(`+168444556677`)', () => {
    // given
    const phoneValue = '+168444556677';

    // test
    const result = service.getCountryByPhoneValue(phoneValue);

    // expect
    expect(result.name).toBe('American Samoa');
  });

  it('should return null when calling getCountryByDialCode(``)', () => {
    // given
    const dialCode = '';

    // test
    const result = service.getCountryByDialCode(dialCode);

    // expect
    expect(result).toBeNull();
  });

  it('should return country United States when calling getCountryByDialCode(`1`)', () => {
    // given
    const dialCode = '1';

    // test
    const result = service.getCountryByDialCode(dialCode);

    // expect
    expect(result.name).toBe('United States');
  });

  it('should return 5 countries when calling searchCountry(`so`)', () => {
    // given
    const phrase = 'so';

    // test
    const result = service.searchCountry(phrase);

    // expect
    expect(result.length).toBe(5);
  });

  it('should return 0 countries when calling searchCountry(`xo`)', () => {
    // given
    const phrase = 'xo';

    // test
    const result = service.searchCountry(phrase);

    // expect
    expect(result.length).toBe(0);
  });

  it('should return 233 countries when calling getAllCountries()', () => {
    // given / test
    const result = service.getAllCountries();

    // expect
    expect(result.length).toBe(233);
  });
});
