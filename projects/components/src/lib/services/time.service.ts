import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  /**
   * Takes in a string with up to four digits with an optional colon and
   * attempts to convert it to the format 9999.
   *
   * It does not ensure that the digits are appropriate for a time value.
   */
  public formatTime(time: string): string {
    time = (time || '').toLocaleLowerCase();
    // Doing the work for am and pm
    if (time.includes('a') || time.includes('p')) {
      if (time.includes('a')) {
        if (time.length === 2 && time.substr(1, 2) === 'a') {
          time = '0' + time.substr(0, 1) + '00';
        } else if (time.length === 3) {
          if (time.substr(1, 3) === 'am') {
            time = '0' + time.substr(0, 1) + '00';
          } else if (time.substr(2, 3) === 'a') {
            if (+time.substr(0, 2) >= 12) {
              time = '';
            } else {
              time = time.substr(0, 2) + '00';
            }
          } else { time = ''; }
        } else if (time.length === 4 && time.substr(2, 4) === 'am') {
          if (+time.substr(0, 2) >= 12) {
            time = '';
          } else {
            time = time.substr(0, 2) + '00';
          }
        } else { time = ''; }
      } else {
        if (time.length === 2 && time.substr(1, 2) === 'p') {
          time = (12 + +time.substr(0, 1)).toString() + '00';
        } else if (time.length === 3) {
          if (time.substr(1, 3) === 'pm') {
            time = (12 + +time.substr(0, 1)).toString() + '00';
          } else if (time.substr(2, 3) === 'p') {
            time = (12 + +time.substr(0, 2)).toString() + '00';
          } else { time = ''; }
        } else if (time.length === 4 && time.substr(2, 4) === 'pm') {
          if (time.substr(0, 2) === '12') {
            time = time.substr(0, 2) + '00';
          } else {
            time = (12 + +time.substr(0, 2)).toString() + '00';
          }
        } else { time = ''; }
      }
      // Doing the work for time with a :
    } else if (time.includes(':')) {
      if (time.substr(0, 1).includes(':')) {
        if (time.length === 1) { time = '00:00'; } else
        if (time.length === 2) { time = '00:' + time.substr(1, 2) + '0'; } else
        if (time.length === 3) { time = '00:' + time.substr(1, 3); } else { time = ''; }
      } else if (time.substr(1, 1).includes(':')) {
        if (time.length === 2) { time = '0' + time.substr(0, 1) + ':00'; } else
        if (time.length === 3) { time = '0' + time.substr(0, 1) + ':' + '0' + time.substr(2, 3); } else
        if (time.length === 4) { time = '0' + time.substr(0, 1) + ':' + time.substr(2, 4); } else {
          time = '';
        }
      } else if (time.substr(2, 2).includes(':')) {
        if (time.length === 3) { time = time.substr(0, 2) + ':00'; } else
        if (time.length === 4) { time = time.substr(0, 3) + '0' + time.substr(3, 4); } else
        if (time.length !== 5) { time = ''; }
      } else { time = ''; }
      // Doing the work for just numbers
    } else {
      if (time.length === 1) { time = '0' + time + '00'; } else
      if (time.length === 2) { time = time + '00'; } else
      if (time.length === 3) {
        if (+time.substr(0, 1) > 2) { time = '0' + time; } else {
          time = time + '0';
        }
      }
    }

    return time.replace(':', '');
  }

  public convertTimeStringToDate(time: string): Date {
    if (this.isValidTime(time)) {
      const hours: number = +time.substr(0, 2);
      const minutes: number = +time.substr(2, 4);
      const date: Date = new Date();
      date.setHours(hours, minutes, 0, 0);

      return date;
    } else {
      return null;
    }
  }

  public convertDateToTimeString(date: Date): string {
    if (!date) {
      return '';
    }

    const hours: number = date.getHours();
    const minutes: number = date.getMinutes();

    return `${this.padToTwoDigits('' + hours)}:${this.padToTwoDigits('' + minutes)}`;
  }

  /**
   * Takes in a 4 digit string ("0934" which means 9:34am) and returns true if it represents
   * a valid time.
   */
  private isValidTime(time: string): boolean {
    return time != null
      && time !== ''
      && time.length === 4
      && Number(time.substring(0, 2)) < 24
      && Number(time.substring(2, 4)) < 60
      && Number(time) <= 2359;
  }

  private padToTwoDigits(value: string): string {
    if (value.length === 1) {
      return '0' + value;
    } else {
      return value;
    }
  }
}
