import {ComponentRef, Injectable, Injector} from '@angular/core';
import {ComponentType, Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplateModalOverlayRef} from './template-modal-overlay.ref';
import {CdkPortal, ComponentPortal} from '@angular/cdk/portal';

class FormModalOverlayRef {
  constructor(overlayRef: any) {

  }

}

class FormModalConfig {
}

@Injectable()
export class TemplateModalService {
  public dialogRef: TemplateModalOverlayRef;
//  public customConfig: CustomOverlayConfig;

  constructor(
    private injector: Injector,
    private overlay: Overlay
  ) {
  }

  open<T>(templateRef: CdkPortal, config: FormModalConfig = {}) {
/*
    // Returns an OverlayRef (which is a PortalHost)
    const modalConfig = { ...DEFAULT_CONFIG, ...config };
    const overlayRef = this.createOverlay(modalConfig);
    const dialogRef = new FormModalOverlayRef(overlayRef);
    const overlayComponent = this.attachModalContainer<T>(overlayRef, modalConfig, dialogRef, component);
    this.dialogRef = dialogRef;
    return dialogRef;
*/
  }

  private attachModalContainer<T>(
    overlayRef: OverlayRef,
    config: FormModalConfig,
    dialogRef: FormModalOverlayRef,
    component: ComponentType<T>
  ) {
 /*   const injector = this.createInjector(config, dialogRef);
    const containerPortal = new ComponentPortal(component, null, injector);
    const containerRef: ComponentRef<T> = overlayRef.attach(containerPortal);
    return containerRef.instance;
 */ }

}
