interface CustomOverlayConfig {
  hasBackdropClick?: boolean;
  isCentered?: boolean;
  size?: any;
  top?: string;
}
