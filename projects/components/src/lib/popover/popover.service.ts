// Ideas from https://netbasal.com/creating-powerful-components-with-angular-cdk-2cef53d81cea

// TODO There is plenty of cleanup to do here.  Some of it is inherited from the above URL which is not very typesafe.

import {Injectable, Injector} from '@angular/core';
import {
  ConnectionPositionPair,
  FlexibleConnectedPositionStrategyOrigin,
  Overlay,
  OverlayConfig,
  PositionStrategy
} from '@angular/cdk/overlay';
import {ComponentPortal, PortalInjector} from '@angular/cdk/portal';
import {PopoverContent, PopoverRef} from './popover-ref';
import {PopoverComponent} from './popover-component/popover.component';

export interface PopoverParams<T> {
  origin: HTMLElement;
  title?: string;
  content: PopoverContent;
//  data?: T;
  width?: string | number;
  height?: string | number;
  positions?: ConnectionPositionPair[];
}

@Injectable()
export class PopoverService {

  constructor(private overlay: Overlay, private injector: Injector) { }

  public static getBottomPositions(): ConnectionPositionPair[] {
    return [{
      originX: 'start',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: 5,
    }];
  }

  open<T>(data: PopoverParams<T>): PopoverRef<T> {

    const config = {
      origin: data.origin,
      width: data.width,
      height: null,
      positions: null,
    };

    if (data.width) {
      config.width = data.width;
    } else {
      config.width = '300px';
    }

    if (data.height) {
      config.height = data.height;
    } else {
      // Let the popover shrink to fit contents.
    }

    config.positions = data.positions ? data.positions : this.getPositions();

    const overlayRef = this.overlay.create(this.getOverlayConfig(config));
    const popoverRef = new PopoverRef<any>(overlayRef, data.content, data);

    const injector = this.createInjector(popoverRef, this.injector);
    overlayRef.attach(new ComponentPortal(PopoverComponent, null, injector));

    return popoverRef;
  }

  createInjector(popoverRef: PopoverRef, injector: Injector) {
    const tokens = new WeakMap([[PopoverRef, popoverRef]]);
    return new PortalInjector(injector, tokens);
  }

  private getOverlayConfig({ origin, width, height, positions }): OverlayConfig {
    return new OverlayConfig({
      width,
      height,
      hasBackdrop: true,
      backdropClass: 'tooltip-mega-backdrop',
      positionStrategy: this.getOverlayPosition(origin, positions),
      scrollStrategy: this.overlay.scrollStrategies.reposition()
    });
  }

  private getOverlayPosition(origin: HTMLElement, positions: ConnectionPositionPair[]): PositionStrategy {
    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo(origin)
      .withPositions(positions)
      .withPush(false);

    return positionStrategy;
  }

  private getPositions(): ConnectionPositionPair[] {
    return [{
      originX: 'end',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'top',
      offsetX: 5,
    }, {
      originX: 'start',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'bottom',
      offsetY: -5,
    }];
  }
}
