import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lrm-popover-content',
  templateUrl: './popover-content.component.html',
  styleUrls: ['./popover-content.component.scss']
})
export class PopoverContentComponent implements OnInit {

  @Input() scrollable = false;

  constructor() { }

  ngOnInit() {
  }

}
