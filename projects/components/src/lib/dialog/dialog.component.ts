import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lrm-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit, AfterViewInit {

  @Input() header: string;
  @Input() visible = false;

  @Input() width: number;

  style: {};

  constructor( private element: ElementRef ) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if ( this.width ) {
//      console.log( 'WIDTH: ' + this.width );
      setTimeout(() => {
        this.style = { width: this.width };
      }, 0 );
    }
  }

}
